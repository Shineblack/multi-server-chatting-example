===============================================================================
    Project Overview
===============================================================================

This is the simple client-server chatting example. This example is to show how the multiple servers are working with multiple clients and some more features such as auto-reconnection, simple load-balancing and the interconnection between multiple servers to share the chatting message to all the connected clients.

1. Idea & Design

There are one network library, one client and two servers for this project.
All the client and servers are using the same library.
The library is only compilable in Visual Studio. However, it is considered to port on the other platform. At this moment it only has I/O multiplexing with select() function with multi-threading.

There are two servers. One is the Master Server, and the other is Message Server.
The Master Server is managing the first connection from the client, containing the information of the Message Servers, make the communication bridge between the Message Servers. It is working as a load-balancing and broadcasting server.

All the communication is using TCP/IP only.
I left all the debug messages on the screen to show what is going on.

2. Compilation

Use Visual Studio 2017.
Compile the network library (HNet) first and other applications next.
Open the solution file from Visual Studio and do the compilation.
However, don't use 'release' configuration because it will cause the issue with the user input from the console. Use 'debug' configuration with 'x86' (Win32) only.

3. Run and test

The steps to run the servers.
(a) Run one 'MasterServer_x86_debug.exe' in the folder 'Server\bin'.
(b) Run multiple (maximum 10) 'MessageServer_x86_debug.exe' in the folder 'Server\bin'.
(c) Run multiple (maximum 639) 'Client_x86_debug.exe' in the folder 'Client\bin'.
(d) Type any text message inside the client and 'Enter' key to send the message to the server.

4. Known bugs and issues

- Compile in 'Release' will not work. Because _kbhit() and _getch() functions which I used to receive the key input in the Client console are not working correctly. However, HNet network library is working with all 'Release' and 'Debug' with any of 'x86' and 'x64' configurations. This is not causing any issue with the network/server.
- The number of Message Server is limited up to 10 only and client connections to the single server is limited up to 64 only. I limit this because of the compatibility between Windows and Linux.
