#ifndef _CLIENTNETWORK_H
#define _CLIENTNETWORK_H

#pragma once

#include "NetLib.h"


///-------------------------
/// Class for client network
namespace HNet
{
    class _ClientNetwork : public NetLib
    {
        private:
            _SessionNode Session;
            _ProcessSessionList ToProcessList;

        public:
            _ClientNetwork();
            ~_ClientNetwork();

        public:
            SOCKET GetSocket( void ) { return GetMainSocket(); }
            _SessionNode *GetSession( void ) { return &Session; }
            _ProcessSessionList *GetProcessList( void ) { return &ToProcessList; }

            char *GetSessionIPAddress( void ) { return Session.GetSessionIPAddress(); };
            unsigned int GetSessionPortNumber( void ) { return Session.GetSessionPort(); };

        public:
            bool InitNet( int SocketModelType, int ProtocolType, const char *ServerIpAddress, int PortNumber );
            int ConnectToServer( const char *ServerIPAddress, int PortNumber );
            int CloseSession( void );
            int SendPacket( struct _PacketMessage PacketMessage );
    };
}

namespace HNet
{
    DWORD WINAPI RunClientThread( void *arg );
}


#endif
