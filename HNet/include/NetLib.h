///////////////////////////////////////////////////////////////////////////////
// ** CAUSION **                                                             //
// YOU SHOULD NOT TOUCH ANYTHING INSIDE THIS HEADER FILE.                    //
///////////////////////////////////////////////////////////////////////////////

#ifndef _HNET_H
#define _HNET_H
#pragma once
#ifndef _WINSOCK_DEPRECATED_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#endif

#include <Ws2tcpip.h>
#include <WinSock2.h>
#include <thread>
#include <list>

#include "Session.h"
#include "ProcessSession.h"

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")


namespace HNet
{
    void debug_print_packet( char Buffer[], int BufferLength );
    void debug_print_fdset( fd_set FdSets );
}


namespace HNet
{
#define PACKET_HEADER           0xFFFB
#define PACKET_HEADER_SIZE      2
#define PACKET_HEADER_DATATYPE  unsigned short int
#define PACKET_TRAILER          0xFFFA
#define PACKET_TRAILER_SIZE     2
#define PACKET_TRAILER_DATATYPE unsigned short int
#define PACKET_LENGTH_SIZE      2
#define PACKET_LENGTH_DATATYPE  unsigned short int
}


namespace HNet
{
    // Error codes for HNet.
    enum
    {
        HNET_ERROR_NOERROR = 0,
        HNET_ERROR_WSASTARTUP,		    // 1
        HNET_ERROR_WRONGAPPTYPE,		    // 2
        HNET_ERROR_SOCKETFAILED,		    // 3
        HNET_ERROR_BINDFAILED,		    // 4
        HNET_ERROR_LISTENFAILED,          // 5
        HNET_ERROR_CREATERECVTHREADFAILD, // 6
        HNET_ERROR_SELECTRETURNEDERROR,   // 7
        HNET_ERROR_ACCEPTFAILED,          // 8
    };

    // App Type.
    enum
    {
        APPTYPE_SERVER = 1,
        APPTYPE_CLIENT
    };

	// Socket Model
	enum
	{
        SOCKET_MODEL_SINGLETHREAD = 0,
		SOCKET_MODEL_SELECT,
		SOCKET_MODEL_IOCP,
		SOCKET_MODEL_EPOLL
	};

    // Protocol Type.
    enum
    {
        PROTOCOL_TCP = 1,
        PROTOCOL_UDP
    };
}

namespace HNet
{
    //-------------------------
    // Class definitions for Network Objects.
    class NetLib
    {
        private:
            int     AppType;
            int     SocketModelType;
            int     ProtocolType;
            int     PortNumber;

            WSADATA WsaData;
            SOCKET  MainSocket;

            HANDLE  RecvThreadHandle;
            DWORD   RecvThreadID;

            int ErrorCode;

        // Constructor and Destructor.
        public:
            NetLib();
            ~NetLib();

        // Getters & Setters.
        protected: // Setters.
            void   SetAppType( int AppType )                 { this->AppType = AppType; }
            void   SetMainSocket( SOCKET Sockfd )            { this->MainSocket = Sockfd; }
            void   SetSocketModelType( int SocketModelType ) { this->SocketModelType = SocketModelType;  }
            void   SetProtocolType( int ProtocolType )       { this->ProtocolType = ProtocolType; }
            void   SetPortNumber( int PortNumber )           { this->PortNumber = PortNumber; }

            void   SetRecvThreadHandle( HANDLE hThread ) { RecvThreadHandle = hThread; }

            void   SetRecvThreadID( DWORD ThreadID ) { RecvThreadID = ThreadID; }

        public: // Getters.
            int    GetAppType( void )         { return this->AppType; }
            SOCKET GetMainSocket( void )      { return this->MainSocket; }
            int    GetSocketModelType( void ) { return this->SocketModelType; }
            int    GetProtocolType( void )    { return this->ProtocolType; }
            int    GetPortNumber( void )      { return this->PortNumber; }

            HANDLE GetRecvThreadHandle( void )           { return RecvThreadHandle; }

            DWORD  GetRecvThreadID( void )           { return RecvThreadID; }

        private:
            int EncodePacket( struct _PacketMessage PacketMessage, char Packet[] );
            int DecodePacket( char Packet[], struct _PacketMessage PacketMessage );

        public:
            void SetErrorCode( int ErrorCode );

            int   GetErrorCode( void );
            char *GetErrorMessage( void );

        protected:
            int InitHNet( int AppType, int SocketModelType, int ProtocolType, int PortNumber = 0);

        // General functions for Winsock.
        public:
            int SendPacket( SOCKET SendSocket, struct _PacketMessage PacketMessage );
    };
}


#endif