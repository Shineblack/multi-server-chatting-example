///////////////////////////////////////////////////////////////////////////////
// ** CAUSION **                                                             //
// YOU SHOULD NOT TOUCH ANYTHING INSIDE THIS HEADER FILE.                    //
///////////////////////////////////////////////////////////////////////////////

#ifndef _SERVERNETWORK_H
#define _SERVERNETWORK_H
#pragma once

#include <thread>

#include "NetLib.h"


//-------------------------
// Class for server network
namespace HNet
{
    class _ServerNetwork : public NetLib
    {
        private:
            _SessionList               SessionList;
            struct _ProcessSessionList ToProcessList;

            std::thread RecvThread;       // Worker thread to receive client message.
            std::thread AliveCheckThread; // Only for UDP communication.
            bool RunRecvThread;
            bool RunAliveCheckThread;

        public:
            _ServerNetwork();
            ~_ServerNetwork();

        public:
            bool InitNet( int SocketModelType, int ProtocolType, int PortNumber, bool BindPort = true );
            int GetConnectedCount( void );
            int CloseSession( _SessionNode *SessionNode );
            int CloseSessionByIndex( int SessionIndex );

            SOCKET GetMainSocket() { return NetLib::GetMainSocket(); }
            _SessionList *GetSessionList() { return &SessionList; }
            _ProcessSessionList *GetProcessList() { return &ToProcessList;  }

            int SendPacket( int SessionIndex, struct _PacketMessage PacketMessage );
            int SendPacketToAll( _PacketMessage PacketMessage );
            int SendPacketToAllExcept( _PacketMessage PacketMessage, int SessionIndex );

        public: // Overloading function
            _SessionNode * GetSessionNodeByIndex( int Index ) { return this->GetSessionList()->GetSessionNodeByIndex( Index ); }

        private: // Worker thread function.
            void TCPServerRecvThread( void );
            void UDPServerRecvThread( void );
            void UDPServerAliveCheckThread( void );
    };
}

#endif
