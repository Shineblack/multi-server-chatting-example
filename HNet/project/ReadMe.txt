========================================================================
    STATIC LIBRARY : HNet Project Overview
========================================================================

AppWizard has created this HNet library project for you.

HNet.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

HNet.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

/////////////////////////////////////////////////////////////////////////////
notes:

This simple network library is only for small number of concurrent connection projects.
It is using C++ 11 thread and select() I/O multiplexing. So the maximun available concurrent
connection is 64 (==FD_SETSIZE) only.
If you want to expend the library, use and replace the network logics with IOCP.

/////////////////////////////////////////////////////////////////////////////
