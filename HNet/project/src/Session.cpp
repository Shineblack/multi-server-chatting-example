#include "NetLib.h"
#include "Session.h"
#include <string.h>


namespace HNet
{
    //----------------------------------
    // Member functions for SessionNode.
    _SessionNode::_SessionNode()
    {
        this->Index = 0;
        this->SessionAddress = { '\0', };

        this->RecvBufferWritePos = 0;

        Prev = nullptr;
        Next = nullptr;
    }

    _SessionNode::~_SessionNode()
    {
        // 세션을 지우지 않기 때문에 여기가 불려질 일은 없다.
    }

    void _SessionNode::SetIndex( int Index )
    {
        this->Index = Index;
    }

    int _SessionNode::GetIndex( void )
    {
        return this->Index;
    }

    void _SessionNode::SetCommSocket( SOCKET CommSocket )
    {
        this->CommSocket = CommSocket;
    }
    SOCKET _SessionNode::GetCommSocket( void )
    {
        return this->CommSocket;
    }

    void _SessionNode::SetSessionAddress( SOCKADDR_IN SessionAddress )
    {
        memcpy( &( this->SessionAddress ), &SessionAddress, sizeof( this->SessionAddress ) );
    }

    SOCKADDR_IN _SessionNode::GetSessionAddress( void )
    {
        return SessionAddress;
    }

    void _SessionNode::SetSessionIPAddress( const char * IPAddress )
    {
        inet_pton( AF_INET, IPAddress, &SessionAddress.sin_addr );
    }

    char * _SessionNode::GetSessionIPAddress( void )
    {
        return inet_ntoa( SessionAddress.sin_addr );
    }

    void _SessionNode::SetSessionPort( unsigned short int SessionPort )
    {
        this->SessionPort = SessionPort;
    }

    unsigned short int _SessionNode::GetSessionPort( void )
    {
        return this->SessionPort;
    }

    void _SessionNode::ClearRecvBuffer( void )
    {
        RecvBufferWritePos = 0;
    }

    int _SessionNode::AddToRecvBuffer( char NewBuffer[], int NewBufferLength )
    {
        if( ( 0 >= NewBufferLength ) || ( RECV_BUFSIZE <= ( RecvBufferWritePos + NewBufferLength ) ) )
        {
            return 0;
        }
        memcpy( &RecvBuffer[RecvBufferWritePos], NewBuffer, NewBufferLength );
        RecvBufferWritePos += NewBufferLength;

        return RecvBufferWritePos;
    }

    char * _SessionNode::GetRecvBuffer( void )
    {
        return (char *)RecvBuffer;
    }

    char * _SessionNode::GetRecvBuffer( int Pos )
    {
        if( RecvBufferWritePos > Pos )
        {
            return (char *)( RecvBuffer + Pos );
        }
        else
        {
            return nullptr;
        }
    }

    int _SessionNode::ShiftRecvBuffer( int NewStartPos )
    {
        if( ( 0 >= NewStartPos ) || ( 0 > ( RecvBufferWritePos - NewStartPos ) ) )
        {
            return 0;
        }
        if( 0 < ( RecvBufferWritePos -= NewStartPos ) )
        {
            memmove( RecvBuffer, &RecvBuffer[NewStartPos], RecvBufferWritePos );
        }
        return RecvBufferWritePos;
    }

    void _SessionNode::SetRecvBufferWritePos( int Pos )
    {
        if( ( 0 > Pos ) || ( RECV_BUFSIZE <= Pos ) )
        {
            return;
        }
        RecvBufferWritePos = Pos;
    }

    int _SessionNode::GetRecvBufferWritePos( void )
    {
        return RecvBufferWritePos;
    }

    int _SessionNode::CheckCompletedPacket( char CompletedPacket[], int *CompletedPacketLength )
    {
        // ** TODO ** Need to check length of the packet.
        // ** TODO ** Make it safe for any condition!

        if( ( PACKET_HEADER_SIZE + PACKET_TRAILER_SIZE + PACKET_LENGTH_SIZE ) >= RecvBufferWritePos ) return 0;

        PACKET_HEADER_DATATYPE  Header;
        PACKET_TRAILER_DATATYPE Trailer;
        PACKET_LENGTH_DATATYPE  PacketLength;

        // Get Packet Header.
        memcpy( &Header, RecvBuffer, PACKET_HEADER_SIZE );
        if( PACKET_HEADER != Header )
        { // Need to find packet header!
            int Pos;
            for( Pos = 1; Pos < RecvBufferWritePos; ++Pos )
            {
                memcpy( &Header, &RecvBuffer[Pos], PACKET_HEADER_SIZE );
                if( PACKET_HEADER == Header )
                {
                    break;
                }
            }
            if( Pos == RecvBufferWritePos )
            { // Error! Reset buffer!
                RecvBufferWritePos = 0;
                return 0;
            }

            ShiftRecvBuffer( Pos );
        }

        // get Packet Length.
        memcpy( &PacketLength, &RecvBuffer[PACKET_HEADER_SIZE], PACKET_LENGTH_SIZE );
        if( ( PACKET_HEADER_SIZE + PACKET_TRAILER_SIZE + PACKET_LENGTH_SIZE ) >= PacketLength )
        { // 뭔가 잘못됐다. 무시해야 한다. 새로운 헤더 찾을 때까지 가야 한다.
            int Pos;
            for( Pos = ( PACKET_HEADER_SIZE + PACKET_LENGTH_SIZE ); Pos < RecvBufferWritePos; ++Pos )
            {
                memcpy( &Header, &RecvBuffer[Pos], PACKET_HEADER_SIZE );
                if( PACKET_HEADER == Header )
                {
                    break;
                }
            }
            if( Pos == RecvBufferWritePos )
            { // Error! Reset buffer!
                RecvBufferWritePos = 0;
                return 0;
            }

            ShiftRecvBuffer( Pos );
        }
        if( RecvBufferWritePos < PacketLength )
        { // 아직 패킷이 모두 도착하지 않았다.
            return 0;
        }

        // get packet trailer.
        memcpy( &Trailer, &RecvBuffer[PacketLength - PACKET_TRAILER_SIZE], PACKET_TRAILER_SIZE );
        if( PACKET_TRAILER != Trailer )
        { // Something Wrong!
            ShiftRecvBuffer( PacketLength );
            return 0;
        }

        // Packet completed!
        *CompletedPacketLength = PacketLength - (PACKET_HEADER_SIZE+PACKET_TRAILER_SIZE+PACKET_LENGTH_SIZE);
        memcpy( CompletedPacket, &RecvBuffer[PACKET_HEADER_SIZE + PACKET_LENGTH_SIZE], *CompletedPacketLength );
        ShiftRecvBuffer( PacketLength );

        return 1;
    }

    void _SessionNode::SetPrev( _SessionNode * Prev )
    {
        this->Prev = Prev;
    }

    _SessionNode * _SessionNode::GetPrev( void )
    {
        return this->Prev;
    }

    void _SessionNode::SetNext( _SessionNode * Next )
    {
        this->Next = Next;
    }

    _SessionNode * _SessionNode::GetNext( void )
    {
        return this->Next;
    }

    void _SessionNode::SetNewTCPConnectedSession( SOCKET Socket, SOCKADDR_IN Address )
    {
        this->SetCommSocket( Socket );
        this->SetSessionAddress( Address );
    }

    void _SessionNode::SetNewUDPConnectedSession( short int SessionPort, SOCKADDR_IN Address )
    {
        this->SetSessionPort( SessionPort );
        this->SetSessionAddress( Address );
    }

    void _SessionNode::Clear( void )
    {
    }

    void _SessionNode::CloseSession( void )
    {
        closesocket( CommSocket );
        Clear();
    }

    //=========================================================================
    // Member functions for SessionList.
    _SessionList::_SessionList()
    {
        int i;

        // Clear IndexList.
        for( i = 0; i < MAX_CONNECTION; ++i )
        {
            IndexList[i] = nullptr;
        }

        // Init ActiveList.
        ActiveList.SetNodeCount( 0 );
        ActiveList.SetHead( nullptr );
        ActiveList.SetHead( nullptr );

        // Init EmptyList. Prepare 'NodeCount' of nodes.
        EmptyList.SetNodeCount( 0 );
        EmptyList.SetHead( nullptr );
        EmptyList.SetHead( nullptr );

        for( i = 0; i < MAX_CONNECTION; ++i )
        {
            _SessionNode *Node = new _SessionNode;
            if( 0 >= EmptyList.AttachNode( Node ) ) break;
            Node->SetIndex( i + 1 );
            IndexList[i + 1] = Node;
        }
    }

    _SessionList::~_SessionList()
    {
    }

    _SessionNode * _SessionList::GetSessionNodeByIndex( int Index )
    {
        return IndexList[Index];
    }

    void _SessionList::_ListData::SetNodeCount( int Count )
    {
        NodeCount = Count;
    }

    void _SessionList::_ListData::SetHead( _SessionNode * Node )
    {
        Head = Node;
    }

    void _SessionList::_ListData::SetTail( _SessionNode * Node )
    {
        Tail = Node;
    }

    int _SessionList::_ListData::GetNodeCount( void )
    {
        return NodeCount;
    }

    _SessionNode * _SessionList::_ListData::GetHead( void )
    {
        return Head;
    }

    _SessionNode * _SessionList::_ListData::GetTail( void )
    {
        return Tail;
    }

    int _SessionList::_ListData::AttachNode( _SessionNode * Node )
    {
        if( ( nullptr == Head ) || ( nullptr == Tail ) )
        {
            Head = Tail = Node;
        }
        else if( Head == Tail )
        {
            Head->SetNext( Node );
            Tail = Node;
            Tail->SetPrev( Head );
        }
        else
        {
            Tail->SetNext( Node );
            Node->SetPrev( Tail );
            Tail = Node;
        }
        ++(NodeCount);

        return NodeCount;
    }

    _SessionNode * _SessionList::_ListData::DetachFirstNode( void )
    {
        if( nullptr == Head )
        {
            return nullptr;
        }

        _SessionNode *Node = Head;
        if( Head == Tail )
        {
            Head = Tail = nullptr;
        }
        else
        {
            Head = Head->GetNext();
            Head->SetPrev( nullptr );
        }
        --(NodeCount);

        Node->SetPrev( nullptr );
        Node->SetNext( nullptr );
        return Node;
    }

    _SessionNode * _SessionList::_ListData::DetachLastNode( void )
    {
        if( nullptr == Tail )
        {
            return nullptr;
        }

        _SessionNode *Node = Tail;
        if( Head == Tail )
        {
            Head = Tail = nullptr;
        }
        else
        {
            Tail = Tail->GetPrev();
        }
        --(NodeCount);

        Node->SetPrev( nullptr );
        Node->SetNext( nullptr );
        return Node;
    }

    int _SessionList::_ListData::DetachNode( _SessionNode * Node )
    {
        if( nullptr == Head )
        {
            return -1;
        }

        if( Head == Node )
        {
            Head = Head->GetNext();
            if( nullptr == Head )
            {
                Head = Tail = nullptr;
            }
            else
            {
                Head->SetPrev( nullptr );
            }
        }
        else if( Tail == Node )
        {
            Tail = Tail->GetPrev();
            if( nullptr == Tail )
            {
                Head = Tail = nullptr;
            }
            else
            {
                Tail->SetNext( nullptr );
            }
        }
        else
        {
            ( Node->GetPrev() )->SetNext( Node->GetNext() );
            ( Node->GetNext() )->SetPrev( Node->GetPrev() );
        }
        --(NodeCount);

        Node->SetPrev( nullptr );
        Node->SetNext( nullptr );
        return NodeCount;
    }

    int _SessionList::_ListData::GetSessionIndexBySocket( SOCKET Socket )
    {
        if( nullptr == Head )
        {
            return -1;
        }

        _SessionNode *Node = Head;
        for( ; nullptr != Node; )
        {
            if( Node->GetCommSocket() == Socket )
            {
                return Node->GetIndex();
            }
            Node = Node->GetNext();
        }

        return 0;
    }

    _SessionNode * _SessionList::_ListData::GetSessionNodeBySocket( SOCKET Socket )
    {
        if( nullptr == Head )
        {
            return nullptr;
        }

        _SessionNode *Node = Head;
        for( ; nullptr != Node; )
        {
            if( Node->GetCommSocket() == Socket )
            {
                return Node;
            }
            Node = Node->GetNext();
        }

        return nullptr;
    }

    _SessionNode *_SessionList::_ListData::GetSessionNodeBySessionPort( short int SessionPort )
    {
        if( nullptr == Head )
        {
            return nullptr;
        }

        _SessionNode *Node = Head;
        for( ; nullptr != Node; )
        {
            if( Node->GetSessionPort() == SessionPort )
            {
                return Node;
            }
            Node = Node->GetNext();
        }

        return NULL;
    }
}
