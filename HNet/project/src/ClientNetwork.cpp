#include "Ws2tcpip.h"
#include "NetLib.h"
#include "ClientNetwork.h"


namespace HNet
{
    _ClientNetwork::_ClientNetwork()
    {
        ToProcessList.Head = ToProcessList.Tail = nullptr;
    }

    _ClientNetwork::~_ClientNetwork()
    {
    }

    bool _ClientNetwork::InitNet( int SocketModelType, int ProtocolType, const char *ServerIPAddress, int PortNumber )
    {
        if( 0 > InitHNet( APPTYPE_CLIENT, SocketModelType, ProtocolType ) )
        {
            return false;
        }

        if( 0 > ConnectToServer( ServerIPAddress, PortNumber ) )
        {
            return false;
        }

        /*
        // TCP NO_DELAY.
        int optval = 1;
        int sock = socket( PF_INET, SOCK_STREAM, 0 );
        setsockopt( sock, IPPROTO_TCP, TCP_NODELAY, (char*)&optval, sizeof( optval ) );
        */

        // Create worker thread.
        DWORD ThreadID;
        HANDLE NetThread = CreateThread( NULL, 0, RunClientThread, (void *)this, 0, &ThreadID );

        ToProcessList.Head = NULL;
        ToProcessList.Tail = NULL;

        return true;
    }

    int _ClientNetwork::ConnectToServer( const char * ServerIPAddress, int PortNumber )
    {
        SOCKADDR_IN ServerAddress;
        int ConnectReturn;

        ///----------------------
        /// The sockaddr_in structure specifies the address family,
        /// IP address, and port of the server to be connected to.
        ServerAddress.sin_family = AF_INET;
        ServerAddress.sin_port = htons( PortNumber );
        inet_pton( AF_INET, ServerIPAddress, &ServerAddress.sin_addr );

        Session.SetSessionAddress( ServerAddress );
        Session.SetCommSocket( GetMainSocket() );

        ///----------------------
        /// Connect to server.
        ConnectReturn = connect( Session.GetCommSocket(), (SOCKADDR*)&ServerAddress, sizeof( ServerAddress ) );
        if( ConnectReturn == SOCKET_ERROR )
        {
            Session.CloseSession();
            WSACleanup();
            return -1;
        }

        return 1;
    }

    int _ClientNetwork::CloseSession( void )
    {
        ToProcessList.Attach( &Session, SESSION_STATE_CLOSEREADY, 0, nullptr );
        Session.CloseSession();
        return 0;
    }

    int _ClientNetwork::SendPacket( struct _PacketMessage PacketMessage )
    {
        return NetLib::SendPacket( this->Session.GetCommSocket(), PacketMessage );
    }
}

namespace HNet
{
    DWORD WINAPI RunClientThread( void *arg )
    {
        _ClientNetwork *NetObj = (_ClientNetwork *)arg;
        _SessionNode *Session = NetObj->GetSession();
        _ProcessSessionList *ToProcessList = NetObj->GetProcessList();
        SOCKET  ConnectSocket = NetObj->GetSocket();
        int RecvReturn;

        char RecvBuffer[RECV_BUFSIZE];

        // Message Loop.
        while( 1 )
        {
            memset( RecvBuffer, '\0', RECV_BUFSIZE );
            RecvReturn = recv( ConnectSocket, RecvBuffer, RECV_BUFSIZE, 0 );
            if( 0 >= RecvReturn )
            { // recv() function returned error.
                NetObj->CloseSession();
                return -1;
            }
            else
            { // Message received.
              // Add received message into session buffer.
                Session->AddToRecvBuffer( RecvBuffer, RecvReturn );
                // check, if there are any completed packet in the buffer.
                //       -> yes, add into ProcessSessionList.
                while( Session->CheckCompletedPacket( RecvBuffer, &RecvReturn ) )
                {
                    ToProcessList->Attach( Session, SESSION_STATE_READPACKET, RecvReturn, RecvBuffer );
                }
            }
        }

        return 1;
    }
}