#include "NetLib.h"
#include "ServerNetwork.h"

namespace HNet
{
    _ServerNetwork::_ServerNetwork()
    {
        ToProcessList.Head = NULL;
        ToProcessList.Tail = NULL;

        RunRecvThread = true;
        RunAliveCheckThread = true;
    }
    _ServerNetwork::~_ServerNetwork()
    {
        if( RecvThread.joinable() )
        {
            RecvThread.join();
        }
        if( AliveCheckThread.joinable() )
        {
            AliveCheckThread.join();
        }
    }

    int _ServerNetwork::CloseSession( _SessionNode * SessionNode )
    {
        // ** TODO ** 세션 리스트에서는 지우지만 프로세스 리스트에서는 지우면 안 된다. 프로세스 리스트에 종료 메시지로 올려야 한다.
        ToProcessList.Attach( SessionNode, SESSION_STATE_CLOSEREADY, 0, nullptr );
        SessionNode->CloseSession();

        return 0;
    }

    int _ServerNetwork::CloseSessionByIndex( int SessionIndex )
    {
        _SessionNode *SessionNode = GetSessionNodeByIndex(SessionIndex);
        ToProcessList.Attach( SessionNode, SESSION_STATE_CLOSEREADY, 0, nullptr );
        SessionNode->CloseSession();
        
        return 0;
    }

    bool _ServerNetwork::InitNet( int SocketModelType, int ProtocolType, int PortNumber, bool BindPort )
    {
        if( 0 > InitHNet( APPTYPE_SERVER, SocketModelType, ProtocolType, PortNumber ) )
        {
            return false;
        }

        RunRecvThread = true;
        switch( ProtocolType )
        {
            case PROTOCOL_TCP :
                switch( SocketModelType )
                {
                    case SOCKET_MODEL_SINGLETHREAD:
                        break;
                    case SOCKET_MODEL_SELECT:
                        RecvThread = std::thread( &_ServerNetwork::TCPServerRecvThread, this );
                        break;
                    case SOCKET_MODEL_IOCP:
                        break;
                    case SOCKET_MODEL_EPOLL:
                        break;
                    default:
                        return false;
                        break;
                }
                break;
            case PROTOCOL_UDP :
                switch( SocketModelType )
                {
                    case SOCKET_MODEL_SINGLETHREAD :
                        RecvThread = std::thread( &_ServerNetwork::UDPServerRecvThread, this );
                        RunAliveCheckThread = true;
                        AliveCheckThread = std::thread( &_ServerNetwork::UDPServerAliveCheckThread, this );
                        break;
                    default :
                        return false;
                        break;
                }
                break;
            default :
                return false;
                break;
        }

        ToProcessList.Head = NULL;
        ToProcessList.Tail = NULL;

        return true;
    }

    int _ServerNetwork::GetConnectedCount( void )
    {
        return this->SessionList.ActiveList.GetNodeCount();
    }

    int _ServerNetwork::SendPacket( int SessionIndex, _PacketMessage PacketMessage )
    {
        _SessionNode *SessionNode = this->SessionList.GetSessionNodeByIndex( SessionIndex );
        if( nullptr == SessionNode )
        {
            return -1;
        }
        return NetLib::SendPacket( SessionNode->GetCommSocket(), PacketMessage );
    }

    int _ServerNetwork::SendPacketToAll( _PacketMessage PacketMessage )
    {
        int SentCount = 0;
        _SessionNode *Session;

        for( Session = SessionList.ActiveList.GetHead(); Session != nullptr; Session = Session->GetNext() )
        {
            NetLib::SendPacket( Session->GetCommSocket(), PacketMessage );
        }

        return SentCount;
    }

    int _ServerNetwork::SendPacketToAllExcept( _PacketMessage PacketMessage, int SessionIndex )
    {
        int SentCount = 0;
        _SessionNode *Session;

        for( Session = SessionList.ActiveList.GetHead(); Session != nullptr; Session = Session->GetNext() )
        {
            if( Session->GetIndex() != SessionIndex )
            {
                NetLib::SendPacket( Session->GetCommSocket(), PacketMessage );
            }
        }

        return SentCount;
    }
}

namespace HNet
{
    void _ServerNetwork::TCPServerRecvThread( void )
    {
        _SessionList *SessionList = GetSessionList();
        _ProcessSessionList *ToProcessList = GetProcessList();

        int     SelectReturn;
        int     RecvReturn;

        fd_set  ReadFds, TempFds;
        TIMEVAL Timeout; // struct timeval timeout;

        SOCKET       ServerSocket = GetMainSocket();
        unsigned int Index;

        SOCKET      ClientSocket;
        SOCKADDR_IN ClientAddress;
        int         ClientAddressLength;
        char        RecvBuffer[RECV_BUFSIZE];

        _SessionNode *NewSession = NULL;

        if( SOCKET_ERROR == listen( ServerSocket, 5 ) )
        {
            SetErrorCode( HNET_ERROR_LISTENFAILED );
            return;
        }

        // Message loop for server.
        while( RunRecvThread )
        {
            FD_ZERO( &ReadFds );
            FD_SET( ServerSocket, &ReadFds );

            Timeout.tv_sec = 0;
            Timeout.tv_usec = 100000; // 0.1 sec

            while( 1 )
            {
                TempFds = ReadFds;

                SelectReturn = select( 0, &TempFds, 0, 0, &Timeout );
                if( SOCKET_ERROR == SelectReturn )
                { // Select() function returned error.
                    SetErrorCode( HNET_ERROR_SELECTRETURNEDERROR );
                    return;
                }
                if( 0 == SelectReturn )
                { // Select() function returned by timeout.
                }
                else if( 0 > SelectReturn )
                {
                    SetErrorCode( HNET_ERROR_SELECTRETURNEDERROR );
                    return;
                }
                else
                {
                    for( Index = 0; Index < TempFds.fd_count; Index++ )
                    {
                        if( TempFds.fd_array[Index] == ServerSocket )
                        { // New connection requested by new client.
                            ClientAddressLength = sizeof( ClientAddress );
                            ClientSocket = accept( ServerSocket, (SOCKADDR *)&ClientAddress, &ClientAddressLength );

                            if( INVALID_SOCKET == ClientSocket )
                            {
                                SetErrorCode( HNET_ERROR_ACCEPTFAILED );
                                continue; // go back to inner message loop.
                            }
                            // ** TODO **
                            // If the connected clients are already over FD_SETSIZE,
                            // need to say sorry to this new client and close the connection.

                            // Get one empty session from empty list, fill up the info and add into active list.
                            if( nullptr == ( NewSession = SessionList->EmptyList.DetachLastNode() ) )
                            {
                                closesocket( ClientSocket );
                                continue; // go back to inner message loop.
                            }
                            NewSession->SetNewTCPConnectedSession( ClientSocket, ClientAddress );
                            SessionList->ActiveList.AttachNode( NewSession );
                            ToProcessList->Attach( NewSession, SESSION_STATE_NEWCONNECTION, 0, nullptr );

                            FD_SET( ClientSocket, &ReadFds );
                        }
                        else
                        { // Something to read from socket.
                            RecvReturn = recv( TempFds.fd_array[Index], RecvBuffer, RECV_BUFSIZE, 0 );

                            // Find SessionNode from ActiveList.
                            _SessionNode *SessionNode = SessionList->ActiveList.GetSessionNodeBySocket( TempFds.fd_array[Index] );
                            if( NULL == SessionNode )
                            { // Cannot find? something wrong!
                                //closesocket(TempFds.fd_array[Index]);
                                CloseSession(SessionNode);
                                FD_CLR( TempFds.fd_array[Index], &ReadFds );
                                continue; // go back to inner message loop.
                            }
                            if( 0 >= RecvReturn )
                            { // Connection closed message has arrived.
                                if( -1 == SessionList->ActiveList.DetachNode( SessionNode ) )
                                {
                                    //closesocket(TempFds.fd_array[Index]);
                                    CloseSession(SessionNode);
                                    FD_CLR( TempFds.fd_array[Index], &ReadFds );
                                    continue; // go back to inner message loop.
                                }
                                CloseSession(SessionNode);
                                SessionList->EmptyList.AttachNode( SessionNode );
                                FD_CLR( TempFds.fd_array[Index], &ReadFds );
                            }
                            else
                            { // Something recevied.
                                // Add received message into session buffer.
                                SessionNode->AddToRecvBuffer( RecvBuffer, RecvReturn );
                                // check, if there are any completed packet in the buffer.
                                //       -> yes, add into ProcessSessionList.
                                while( SessionNode->CheckCompletedPacket( RecvBuffer, &RecvReturn) )
                                {
                                    ToProcessList->Attach( SessionNode, SESSION_STATE_READPACKET, RecvReturn, RecvBuffer);
                                }
                            }
                        }
                    }
                }
            }
        }

        return;
    }

    void _ServerNetwork::UDPServerRecvThread( void )
    {
        _SessionList *SessionList = GetSessionList();
        _ProcessSessionList *ToProcessList = GetProcessList();

        SOCKADDR_IN SenderAddr;
        int         SenderAddrSize;
        char        RecvBuffer[RECV_BUFSIZE];
        int         RecvReturn;
        unsigned short int   SessionPort;

        _SessionNode *SessionNode = NULL;

        while( RunRecvThread )
        {
            // Call the recvfrom function to receive datagrams on the bound socket.
            memset( RecvBuffer, '\0', RECV_BUFSIZE );
            SenderAddrSize = sizeof( SenderAddr );
            RecvReturn = recvfrom( GetMainSocket(), RecvBuffer, RECV_BUFSIZE, 0, (SOCKADDR *)&SenderAddr, &SenderAddrSize );
            if( SOCKET_ERROR == RecvReturn )
            { // Connection Error!
                break;
            }
            else if( 0 == RecvReturn )
            { // Connection Closed!
                break;
            }

            SessionPort = ntohs( SenderAddr.sin_port );
            if( NULL == (SessionNode = SessionList->ActiveList.GetSessionNodeBySessionPort( SessionPort )) )
            { // Not in the list. New connection.
              // Get one empty session from empty list, fill up the info and add into active list.
                if( nullptr == ( SessionNode = SessionList->EmptyList.DetachLastNode() ) )
                {
                    continue; // go back to message loop.
                }
                SessionNode->SetNewUDPConnectedSession( SessionPort, SenderAddr );
                SessionList->ActiveList.AttachNode( SessionNode );
            }

            // Add received message into session buffer.
            SessionNode->AddToRecvBuffer( RecvBuffer, RecvReturn );
            // check, if there are any completed packet in the buffer.
            //       -> yes, add into ProcessSessionList.
            while( SessionNode->CheckCompletedPacket( RecvBuffer, &RecvReturn ) )
            {
                ToProcessList->Attach( SessionNode, SESSION_STATE_READPACKET, RecvReturn, RecvBuffer );
            }
        }

        return;
    }

    void _ServerNetwork::UDPServerAliveCheckThread( void )
    {
        // ** TODO ** Need to implement!
        return;
    }
}