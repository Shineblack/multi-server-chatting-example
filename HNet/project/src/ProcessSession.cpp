#include "NetLib.h"
#include "ProcessSession.h"

namespace HNet
{
    void _ProcessSessionList::Attach( _SessionNode *SessionNode, int State, int PacketLength, char Packet[] )
    {
        struct _ProcessSession *ProcessSession = new struct _ProcessSession;
        ProcessSession->SessionState = State;
        ProcessSession->SessionIndex = SessionNode->GetIndex();
        memcpy( &( ProcessSession->SessionAddress ), &( SessionNode->GetSessionAddress() ), sizeof( ProcessSession->SessionAddress ) );
        ProcessSession->PacketMessage.BufferWritePos = PacketLength;
        memcpy( ProcessSession->PacketMessage.Buffer, Packet, PacketLength );

        if( ( nullptr == Head ) && ( nullptr == Tail ) )
        {
            Head = Tail = ProcessSession;
            ProcessSession->Prev = ProcessSession->Next = nullptr;
        }
        else
        {
            Tail->Next = ProcessSession;
            ProcessSession->Prev = Tail;
            ProcessSession->Next = nullptr;
            Tail = ProcessSession;
        }
    }

    struct _ProcessSession *_ProcessSessionList::GetFirstSession( void )
    {
        return Head;
    }

    void _ProcessSessionList::DeleteFirstSession( void )
    {
        if( ( nullptr == Head ) && ( nullptr == Tail ) )
        {
            return;
        }

        struct _ProcessSession *SessionToDelete = Head;
        Head = Head->Next;
        if( nullptr == Head )
        {
            Tail = nullptr;
        }
        else
        {
            Head->Prev = nullptr;
        }

        if( nullptr != SessionToDelete->PacketMessage.Buffer )
        {
            SessionToDelete->PacketMessage.Reset();
        }
        delete SessionToDelete;
    }

    int _PacketMessage::DecodePacket( char Packet[], int PacketLength, _PacketMessage *PacketMessage )
    {
        if( 0 >= PacketLength )
        {
            return 0;
        }

        PacketMessage->BufferWritePos = PacketLength - ( PACKET_HEADER_SIZE + PACKET_TRAILER_SIZE + PACKET_LENGTH_SIZE );
        memcpy( PacketMessage->Buffer, &Packet[PACKET_HEADER_SIZE + PACKET_LENGTH_SIZE], PacketMessage->BufferWritePos );

        return PacketMessage->BufferWritePos;
    }

    int _PacketMessage::CopyBufferTo( char *Dest )
    {
        if( nullptr == Dest ) return 0;

        memcpy( Dest, Buffer, BufferWritePos );
        return BufferWritePos;
    }

    int _PacketMessage::CopyBufferFrom( char *Origin, int OriginLength )
    {
        if( 0 >= OriginLength ) return OriginLength;
        if( nullptr == Origin ) return 0;

        memcpy( Buffer, Origin, OriginLength );
        BufferWritePos = OriginLength;
        return OriginLength;
    }
}