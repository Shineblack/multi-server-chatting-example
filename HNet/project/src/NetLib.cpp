// Simple Network library
// 

#include "NetLib.h"

// Global Variables.
namespace HNet
{
    char *g_ErrorMsgText[] = {
        "No Error!",												// 0
        "Error returned from WSAStartUp function",					// 1
        "Wrong applicationt type assigned into initfunction",		// 2
        "Socket function failed",									// 3
        "Bind function failed",										// 4
        "Listen function failed",                                   // 5
        "Create recv function failed",                              // 6
        "Select function returned error",                           // 7
        "Accept function returned error",                           // 8
    };
}

// Global Functions.
namespace HNet
{
    void debug_print_packet( char Buffer[], int BufferLength )
    {
        printf( "\n@ Debug: %s", __FUNCTION__ );
        if( 0 > BufferLength )
        {
            printf( "\n@ BufferLength is below 0" );
            return;
        }

        printf( "\n@ Packet Buffer: " );
        for( int i = 0; i < BufferLength; ++i )
        {
            printf( "%02X ", (unsigned char)Buffer[i] );
        }
    }

    void debug_print_fdset( fd_set FdSets )
    {
        printf( "\n@ Debug: fd_count[%d]", FdSets.fd_count );
        if( 0 < FdSets.fd_count )
        {
            printf( ", SetList[" );
            unsigned int i = 0;
            while( i < FdSets.fd_count )
            {
                printf( "%ld", (unsigned int)FdSets.fd_array[i++] );
                if( i >= FdSets.fd_count )
                {
                    printf( "]" );
                    break;
                }
                printf( ", " );
            }
        }
    }
}

namespace HNet
{
    // Constructor and Destructor.
    NetLib::NetLib()
    {
        AppType = 0;
        ProtocolType = 0;
        PortNumber = 0;
        ErrorCode = HNET_ERROR_NOERROR;
        MainSocket = 0;
    }

    NetLib::~NetLib()
    {
    }

    // protected: Getters & Setters.
    void NetLib::SetErrorCode( int ErrorCode )
    {
        this->ErrorCode = ErrorCode;
    }
    
    // public: Getters & Setters.
    int NetLib::GetErrorCode( void )
    {
        return this->ErrorCode;
    }
    char *NetLib::GetErrorMessage( void )
    {
        return g_ErrorMsgText[this->ErrorCode];
    }

    // Encode & Decode the packet.
    int NetLib::EncodePacket( struct _PacketMessage PacketMessage, char Packet[] )
    {
        PACKET_HEADER_DATATYPE  PacketHeader = PACKET_HEADER;
        PACKET_TRAILER_DATATYPE PacketTrailer = PACKET_TRAILER;
        PACKET_LENGTH_DATATYPE  PacketLength = ( PACKET_HEADER_SIZE + PACKET_TRAILER_SIZE + PACKET_LENGTH_SIZE + PacketMessage.BufferWritePos );
        int Offset = 0;

        memcpy( Packet, &PacketHeader, PACKET_HEADER_SIZE );
        Offset += PACKET_HEADER_SIZE;
        memcpy( &Packet[Offset], &PacketLength, PACKET_LENGTH_SIZE );
        Offset += PACKET_LENGTH_SIZE;
        memcpy( &Packet[Offset], PacketMessage.Buffer, PacketMessage.BufferWritePos );
        Offset += PacketMessage.BufferWritePos;
        memcpy( &Packet[Offset], &PacketTrailer, PACKET_TRAILER_SIZE );
        Offset += PACKET_TRAILER_SIZE;

        PacketMessage.BufferWritePos = PacketLength;

        return PacketLength;
    }

    int NetLib::DecodePacket( char Packet[], struct _PacketMessage PacketMessage )
    {
        return 0;
    }

    // General functions for Winsock.
    int NetLib::InitHNet( int AppType, int SocketModelType, int ProtocolType, int PortNumber )
    {
        SOCKADDR_IN Address;

        this->AppType      = AppType;
        this->ProtocolType = ProtocolType;
        this->PortNumber   = PortNumber;

        if( WSAStartup( MAKEWORD( 2, 2 ), &WsaData ) != 0 )
        {
            return -1;
        }

        switch( SocketModelType )
        {
            case SOCKET_MODEL_SINGLETHREAD :
                break;
            case SOCKET_MODEL_SELECT :
                break;
            case SOCKET_MODEL_IOCP :
                break;
            case SOCKET_MODEL_EPOLL :
                break;
            default :
                return -1;
                break;
        }
        SetSocketModelType( SocketModelType );

        switch( ProtocolType )
        {
            case PROTOCOL_TCP:
                MainSocket = socket( AF_INET, SOCK_STREAM, 0 );
                break;
            case PROTOCOL_UDP:
                MainSocket = socket( AF_INET, SOCK_DGRAM, 0 );
                break;
            default:
                ErrorCode = HNET_ERROR_WRONGAPPTYPE;
                return -1;
                break;
        }
        if( INVALID_SOCKET == MainSocket )
        {
            ErrorCode = HNET_ERROR_SOCKETFAILED;
            return -1;
        }
        SetProtocolType( ProtocolType );

        if( APPTYPE_SERVER == AppType )
        {
            Address.sin_family = AF_INET;
            Address.sin_port = htons( PortNumber );
            Address.sin_addr.s_addr = htonl( INADDR_ANY );
            if( SOCKET_ERROR == bind( MainSocket, (SOCKADDR *)&Address, sizeof( Address ) ) )
            {
                ErrorCode = HNET_ERROR_BINDFAILED;
                return -11;
            }
        }
        SetAppType( AppType );

        return 1;
    }

    int NetLib::SendPacket( SOCKET SendSocket, _PacketMessage PacketMessage )
    {
        if( SEND_BUFSIZE <= PacketMessage.GetBufferLength() )
        {
            return 0;
        }

        char Packet[SEND_BUFSIZE];
        int  SendBytes;

        PacketMessage.BufferWritePos = EncodePacket( PacketMessage, Packet );
        SendBytes = send( SendSocket, Packet, PacketMessage.BufferWritePos, 0 );

        return SendBytes;
    }
}


