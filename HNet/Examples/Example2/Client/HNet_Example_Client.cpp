///////////////////////////////////////////////////////////////////////////////
// ** EXAMPLE of how to use NetLib.lib for client application.               //
// You can look around and modify anything.                                  //
// But please follow the guided sequence.                                    //
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <conio.h>  // to use _kbhit()
// Step 1. Include the header file for client.
#include "ClientNetwork.h"
#include "..\Shares\packet_c2s.h"
#include "..\Shares\packet_s2c.h"

using namespace std;

// Step 2. Client network object to use network library.
HNet::_ClientNetwork NetObj;

int main( void )
{
    // Step 3. Initialize the network object with necessary information.
    //         - First parameter:  define an application type.
    //                              > APPTYPE_SERVER for server application.
    //                              > APPTYPE_CLIENT for client application.
    //         - Second parameter: protocol type that we are going to use for communication.
    //                              > PROTOCOL_TCP is for TCP communication.
    //                              > PROTOCOL_UDP is for UDP communication.
    //         - Third parameter:  not using.
    if( false == NetObj.InitNet( HNet::APPTYPE_CLIENT, HNet::PROTOCOL_TCP, "127.0.0.1", 3456 ) )
    {
        // If any error, you can check the error message from this function.
        // If you only want to check the error code instead of actuall string message, you can use GetErrorCode() function.
        cout << NetObj.GetErrorMessage();
        return 0;
    }

    // Step 4. Prepare the pointer of _ProcessSession and buffer structure of _PacketMessage.
    //         _ProcessSession pointer will give you the session information if there is any network communication from any client.
    //         _PacketMessage is for fatch the each of the actual data inside the packet buffer.
    struct HNet::_ProcessSession *ToProcessSessoin;

    // Render Loop. Client usually has a render loop and this is the render loop for the client.
    while( 1 )
    {
        Sleep( 100 ); // Prevent too fast looping. If you has a FPS (Frame Per Second) calculation, replace it here (or at the end of this loop).

        // Step 5. Message Loop.
        //         Check any message from server and process.
        while( nullptr != ( ToProcessSessoin = NetObj.GetProcessList()->GetFirstSession() ) )
        { // Something recevied from network.
            int PacketID;

            switch( ToProcessSessoin->SessionState )
            {
                case HNet::SESSION_STATE_CLOSEREADY:
                {
                    printf( "\nConnection Closed!\n" );
                    return 0;
                }
                case HNet::SESSION_STATE_READPACKET:
                {
                    ToProcessSessoin->PacketMessage >> PacketID;
                    switch( PacketID )
                    {
                        case PACKET_ID_S2C_EXAMPLE1:
                        {
                            struct _Pkt_S2C_Example1 PktData;
                            ToProcessSessoin->PacketMessage >> PktData;
                            printf( "\n Received Example1: short int:%d, char:%c, float:%f, int:%d",
                                    PktData.short_number, PktData.single_letter, PktData.single_float, PktData.single_number );

                            break;
                        }

                        case PACKET_ID_S2C_EXAMPLE2:
                        {
                            struct _Pkt_S2C_Example2 PktData;
                            ToProcessSessoin->PacketMessage >> PktData;
                            printf( "\n Received Example2: int:%d, cstring:%s, char:%c",
                                    PktData.single_number, PktData.string_data, PktData.single_letter );

                            break;
                        }

                        default:
                        {
                            break;
                        }
                    }
                    break;
                }

                default:
                    break;
            }

            // Step 6. After finish the process with packet, You should delete the session message.
            //         If you forget to delete, it will cause memory leak!
            NetObj.GetProcessList()->DeleteFirstSession();

            printf( "\nInput 1 or 2 to test packet: " );
        }

        // From here, you can start your own randering and game process.
        // Add (modify) your codes and process your randering. Also add the codes for whatever you need to do.
        if( _kbhit() )
        { // This block is a example how to make a packet and send it to the server.
            char InputKey = _getch();
            switch( InputKey )
            {
                case '1':
                {
                    struct _Pkt_C2S_Example1 PktData;
                    // We are using structure here so assigning sequence is not important.
                    PktData.single_float  = 34.56f;
                    PktData.single_letter = 'C';
                    PktData.single_number = 876;

                    // Now build the packet and send to the server.
                    struct HNet::_PacketMessage Packet;
                    Packet << PACKET_ID_C2S_EXAMPLE1; // Add the packet ID
                    Packet << PktData;                // Add the structure of data into the packet
                    int SendReturn = NetObj.SendPacket( Packet );
                    printf( "\n%d bytes sent to server.", SendReturn );

                    break;
                }

                case '2':
                {
                    struct _Pkt_C2S_Example2 PktData;
                    PktData.short_number = 369;
                    strcpy_s( PktData.string_data, "String Message" );

                    struct HNet::_PacketMessage Packet;
                    Packet << PACKET_ID_C2S_EXAMPLE2; // Add the packet ID
                    Packet << PktData;                // Add the structure of data into the packet
                    int SendReturn = NetObj.SendPacket( Packet );
                    printf( "\n%d bytes sent to server.", SendReturn );

                    break;
                }

                default:
                {
                    printf( "\nWrong input!" );
                    break;
                }
            }
        }
    }


    return 0;
}