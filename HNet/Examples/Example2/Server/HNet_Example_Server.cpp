///////////////////////////////////////////////////////////////////////////////
// ** EXAMPLE of how to use NetLib.lib for server application.               //
// You can look around and modify anything.                                  //
// But please follow the guided sequence.                                    //
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
// Step 1. Include the header file for server.
#include "ServerNetwork.h"
#include "..\Shares\packet_c2s.h"
#include "..\Shares\packet_s2c.h"

using namespace std;

// Step 2. Server network object to use network library.
HNet::_ServerNetwork NetObj;

int main( void )
{
    // Step 3. Initialize the network object with necessary information.
    //         - First parameter:  define an application type.
    //                              > APPTYPE_SERVER for server application.
    //                              > APPTYPE_CLIENT for client application.
    //         - Second parameter: protocol type that we are going to use for communication.
    //                              > PROTOCOL_TCP is for TCP communication.
    //                              > PROTOCOL_UDP is for UDP communication.
    //         - Third parameter:  port number.
    if( false == NetObj.InitNet( HNet::APPTYPE_SERVER, HNet::PROTOCOL_TCP, 3456 ) )
    {
        // If any error, you can check the error message from this function.
        // If you only want to check the error code instead of actuall string message, you can use GetErrorCode() function.
        cout << NetObj.GetErrorMessage();
        return 0;
    }
    cout << " Server network initialized!" << endl;
    cout << " Network thread started! Ready to accept & recevie the message." << endl;

    // Step 4. Prepare the pointer of _ProcessSession.
    //         This will give you the session information if there is any network communication from any client.
    struct HNet::_ProcessSession *ToProcessSessoin;

    // Step 5. Message Loop.
    //         Server usually don't have a render loop so this message loop is the main body of the server process.
    while( 1 )
    {
        // Step 6. Check for any session to process.
        //         Check if there is any session we need to process.
        //         Three case could be happen.
        //           1. New connected requested from new client.
        //           2. Connection closed or connection error of client.
        //           3. Any packet data received.
        //         If nothing to process, GetFirstSession() will reurn nullptr (=NULL).
        while( nullptr != ( ToProcessSessoin = NetObj.GetProcessList()->GetFirstSession() ) )
        {
            switch( ToProcessSessoin->SessionState )
            {
                case HNet::SESSION_STATE_NEWCONNECTION:
                {
                    // New connection request arrived.
                    printf( "\n New connection connected: Index:%d. Total Connection now:%d", ToProcessSessoin->SessionIndex, NetObj.GetConnectedCount() );

                    // This is the example how you can send the packet to client.
                    struct HNet::_PacketMessage PacketData;       // Create the _PacketMessage structure object.
                    PacketData << "Your session ID is ";           // Add string data into buffer.
                    PacketData << ToProcessSessoin->SessionIndex;  // Add number (integer) data into buffer.
                    int SendReturn = NetObj.SendPacket( ToProcessSessoin->SessionIndex, PacketData ); // send buffer to 

                    break;
                }

                case HNet::SESSION_STATE_CLOSEREADY:
                {
                    // Connection closed arrived or communication error.
                    printf( "\n Received: Index %d wants to close or already closed.\n Total Connection now:%d",
                            ToProcessSessoin->SessionIndex, NetObj.GetConnectedCount() );
                    break;
                }

                case HNet::SESSION_STATE_READPACKET:
                {
                    // Any packet data recevied.
                    int PacketID;

                    ToProcessSessoin->PacketMessage >> PacketID;
                    switch( PacketID )
                    {
                        case PACKET_ID_C2S_EXAMPLE1:
                        {
                            struct _Pkt_C2S_Example1 RecvPacket;
                            ToProcessSessoin->PacketMessage >> RecvPacket;
                            printf( "\n Recevied Example1: char:%c, int:%d, float:%f", RecvPacket.single_letter, RecvPacket.single_number, RecvPacket.single_float );

                            // Echo back PACKET_ID_S2C_EXAMPLE1 to client.
                            struct _Pkt_S2C_Example1 SendData;
                            // We are using structure here so assigning sequence is not important.
                            SendData.short_number  = 246;
                            SendData.single_float  = 95.12f;
                            SendData.single_letter = 'E';
                            SendData.single_number = 789;

                            // Now build the packet and send to the server.
                            struct HNet::_PacketMessage SendPkt;
                            SendPkt << PACKET_ID_S2C_EXAMPLE1; // Add the packet ID
                            SendPkt << SendData;               // Add the structure of data into the packet
                            int SendReturn = NetObj.SendPacket( ToProcessSessoin->SessionIndex, SendPkt );
                            printf( "\n%d bytes sent to server.", SendReturn );

                            break;
                        }

                        case PACKET_ID_C2S_EXAMPLE2:
                        {
                            struct _Pkt_C2S_Example2 RecvPacket;
                            ToProcessSessoin->PacketMessage >> RecvPacket;
                            printf( "\n Recevied Example2: cstring:%s, short int:%d", RecvPacket.string_data, RecvPacket.short_number );

                            // Echo back PACKET_ID_S2C_EXAMPLE2 to client.
                            struct _Pkt_S2C_Example2 SendData;
                            // We are using structure here so assigning sequence is not important.
                            SendData.single_letter = 'X';
                            SendData.single_number = 1024;
                            strcpy_s( SendData.string_data, "This is string data." );

                            // Now build the packet and send to the server.
                            struct HNet::_PacketMessage SendPkt;
                            SendPkt << PACKET_ID_S2C_EXAMPLE2; // Add the packet ID
                            SendPkt << SendData;               // Add the structure of data into the packet
                            int SendReturn = NetObj.SendPacket( ToProcessSessoin->SessionIndex, SendPkt );
                            printf( "\n%d bytes sent to server.", SendReturn );

                            break;
                        }

                        default:
                        {
                            printf( "\n Something wrong. Cannot identify the Packet ID. Now the Packet ID is %d", PacketID );
                            break;
                        }
                    }

                    break;
                }

                default:
                    break;
            }

            // Step 7. After finish the process with packet, You should delete the session message.
            //         If you forget to delete, it will cause memory leak!
            NetObj.GetProcessList()->DeleteFirstSession();
        }

        // You can add your own server codes here. If there is any regular work.

        Sleep( 1000 );
    }

    return 0;
}