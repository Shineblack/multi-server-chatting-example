#ifndef _PACKET_S2C_H
#define _PACKET_S2C_H
#pragma once


/**
* This file defines the packet details of Server to Client packets.
* The Server uses this packet to send message to the Client.
*/


/**
* This is the list of Packet ID.
*/
enum {
    PACKET_ID_S2C_NONE = 0,     // Start of the packet ID. Do not remove this.

    // You can add your own Packet ID here.
    PACKET_ID_S2C_EXAMPLE1,     // Example of packet ID.
    PACKET_ID_S2C_EXAMPLE2,     // Example of packet ID.

    PACKET_ID_S2C_MAX           // This is the end of list. Do not add any packet ID after here.
};


/**
* This is the list of data structures for each packet.
* ** IMPORTANT ** All the structures have to be fixed size.
*/
#pragma pack(push, 1) // Pack the memory alignment. 
struct _Pkt_S2C_Example1 {
    short int short_number;
    char      single_letter;
    float     single_float;
    int       single_number;
};

struct _Pkt_S2C_Example2 {
#define PKT_S2C_EXAMPLE2_STRING_DATA_MAX    35
    int  single_number;
    char string_data[PKT_S2C_EXAMPLE2_STRING_DATA_MAX];
    char single_letter;

    _Pkt_S2C_Example2()
    {
        memset( string_data, '\0', PKT_S2C_EXAMPLE2_STRING_DATA_MAX );
    }
};
#pragma pack(pop) // Return back to default memory alignment.


#endif
