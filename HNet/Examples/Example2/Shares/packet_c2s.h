#ifndef _PACKET_C2S_H
#define _PACKET_C2S_H
#pragma once


/**
* This file defines the packet details of Client to Server packets.
* The Client uses this packet to send message to the Server.
*/


/**
* This is the list of Packet ID.
*/
enum {
    PACKET_ID_C2S_NONE = 0,     // Start of the packet ID. Do not remove this.

    // You can add your own Packet ID here.
    PACKET_ID_C2S_EXAMPLE1,     // Example of packet ID.
    PACKET_ID_C2S_EXAMPLE2,     // Example of packet ID.

    PACKET_ID_C2S_MAX           // This is the end of list. Do not add any packet ID after here.
};


/**
* This is the list of data structures for each packet.
* ** IMPORTANT ** All the structures have to be fixed size.
*/
#pragma pack(push, 1) // Pack the memory alignment. 
struct _Pkt_C2S_Example1 {
    char  single_letter;
    int   single_number;
    float single_float;
};

struct _Pkt_C2S_Example2 {
#define PKT_C2S_EXAMPLE2_STRING_DATA_MAX    25
    char string_data[PKT_C2S_EXAMPLE2_STRING_DATA_MAX];
    short int short_number;
};
#pragma pack(pop) // Return back to default memory alignment.


#endif
