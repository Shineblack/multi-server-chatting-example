///////////////////////////////////////////////////////////////////////////////
// ** EXAMPLE of how to use NetLib.lib for server application.               //
// You can look around and modify anything.                                  //
// But please follow the guided sequence.                                    //
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
// Step 1. Include the header file for server.
#include "ServerNetwork.h"

using namespace std;

// Step 2. Server network object to use network library.
HNet::_ServerNetwork NetObj;

int main( void )
{
    // Step 3. Initialize the network object with necessary information.
    //         - First parameter:  define an application type.
    //                              > APPTYPE_SERVER for server application.
    //                              > APPTYPE_CLIENT for client application.
    //         - Second parameter: protocol type that we are going to use for communication.
    //                              > PROTOCOL_TCP is for TCP communication.
    //                              > PROTOCOL_UDP is for UDP communication.
    //         - Third parameter:  port number.
    if( false == NetObj.InitNet( HNet::APPTYPE_SERVER, HNet::PROTOCOL_TCP, 3456 ) )
    {
        // If any error, you can check the error message from this function.
        // If you only want to check the error code instead of actual string message, you can use GetErrorCode() function.
        cout << NetObj.GetErrorMessage();
        return 0;
    }
    cout << " Server network initialized! Ready to accept & recevie the message." << endl;

    // Step 4. Prepare the pointer of _ProcessSession.
    //         This will give you the session information if there is any network communication from any client.
    struct HNet::_ProcessSession *ToProcessSessoin;

    // (for test) Prepare variables to store the data from packet.
    int Integer;
    int IntNumber;
    float FloatNumber; // Becareful to use float number in the network because of floating error.
    // (commnets) Let's say if you want to send float number 56.78f and did 'float FloatNumber = 56.78f;'
    //            Actually inside the FloatNumber will be not exactlly 56.78. It will be 56.7799988 because of floating error.
    //            After transmit of the number, you will don't know the original number because you received 56.7788877 instead of 56.78
    char StringData[BUFSIZ]; // use 'char' data type instead of 'string' to transmit your string.

    // Step 5. Message Loop.
    //         Server usually don't have a render loop so this message loop is the main body of the server process.
    while( 1 )
    {

        // Step 6. Check for any session to process.
        //         Check if there is any session we need to process.
        //         Three case could be happen.
        //           1. New connected requested from new client.
        //           2. Connection closed or connection error of client.
        //           3. Any packet data received.
        //         If nothing to process, GetFirstSession() will reurn nullptr (=NULL).
        while( nullptr != ( ToProcessSessoin = NetObj.GetProcessList()->GetFirstSession() ) )
        {
            switch( ToProcessSessoin->SessionState )
            {
                case HNet::SESSION_STATE_NEWCONNECTION:
                {
                    // New connection request arrived.
                    printf( "\n New connection connected: Index:%d. Total Connection now:%d", ToProcessSessoin->SessionIndex, NetObj.GetConnectedCount() );

                    // This is the example how you can send the packet to client.
                    struct HNet::_PacketMessage PacketData;       // Create the _PacketMessage structure object.
                    PacketData << "Your session ID is ";           // Add string data into buffer.
                    PacketData << ToProcessSessoin->SessionIndex;  // Add number (integer) data into buffer.
                    int SendReturn = NetObj.SendPacket( ToProcessSessoin->SessionIndex, PacketData ); // send buffer to 

                    break;
                }

                case HNet::SESSION_STATE_CLOSEREADY:
                {
                    // Connection closed arrived or communication error.
                    printf( "\n Connection Closed Detected: Index %d wants to close or already closed.\n Total Connection now:%d",
                            ToProcessSessoin->SessionIndex, NetObj.GetConnectedCount() );
                    break;
                }

                case HNet::SESSION_STATE_READPACKET:
                {
                    // Any packet data recevied.
                    ToProcessSessoin->PacketMessage >> Integer;
                    ToProcessSessoin->PacketMessage >> IntNumber;
                    ToProcessSessoin->PacketMessage >> FloatNumber;
                    ToProcessSessoin->PacketMessage >> StringData;
                    printf( "\n Packet Received: Int:%d, Int:%d, Float:%f, String:%s", Integer, IntNumber, FloatNumber, StringData );

                    // This is the example how you can send the packet to client.
                    struct HNet::_PacketMessage PacketData;       // Create the _PacketMessage structure object.
                    PacketData << "Server echo your number: ";
                    PacketData << IntNumber;
                    int SendReturn = NetObj.SendPacket( ToProcessSessoin->SessionIndex, PacketData ); // send buffer to 

                    break;
                }

                default:
                    break;
            }

            // Step 7. After finish the process with packet, You should delete the session message.
            //         If you forget to delete, it will cause memory leak!
            NetObj.GetProcessList()->DeleteFirstSession();
        }

        // You can add your own server codes here. If there is any regular work.

        Sleep( 1000 );
    }

    return 0;
}