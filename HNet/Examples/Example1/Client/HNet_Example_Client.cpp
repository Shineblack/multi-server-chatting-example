///////////////////////////////////////////////////////////////////////////////
// ** EXAMPLE of how to use NetLib.lib for client application.               //
// You can look around and modify anything.                                  //
// But please follow the guided sequence.                                    //
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
// Step 1. Include the header file for client.
#include "ClientNetwork.h"

using namespace std;

// Step 2. Client network object to use network library.
HNet::_ClientNetwork NetObj;

int main( void )
{
    // Step 3. Initialize the network object with necessary information.
    //         - First parameter:  define an application type.
    //                              > APPTYPE_SERVER for server application.
    //                              > APPTYPE_CLIENT for client application.
    //         - Second parameter: protocol type that we are going to use for communication.
    //                              > PROTOCOL_TCP is for TCP communication.
    //                              > PROTOCOL_UDP is for UDP communication.
    //         - Third parameter:  IP Address of server.
    //         - Forth parameter:  Port number of server.
    if( false == NetObj.InitNet( HNet::APPTYPE_CLIENT, HNet::PROTOCOL_TCP, "127.0.0.1", 3456 ) )
    {
        // If any error, you can check the error message from this function.
        // If you only want to check the error code instead of actuall string message, you can use GetErrorCode() function.
        cout << NetObj.GetErrorMessage();
        return 0;
    }

    // Step 4. Prepare the pointer of _ProcessSession and buffer structure of _PacketMessage.
    //         _ProcessSession pointer will give you the session information if there is any network communication from any client.
    //         _PacketMessage is for fetch the each of the actual data inside the packet buffer.
    struct HNet::_ProcessSession *ToProcessSessoin;
    struct HNet::_PacketMessage PacketData;

    // Render Loop. Client usually has a render loop and this is the render loop for the client.
    while( 1 )
    {
        Sleep( 1000 ); // Prevent too fast looping. If you has a FPS (Frame Per Second) calculation, replace it here (or at the end of this loop).

        // Step 5. Message Loop.
        //         Check any message from server and process.
        while( nullptr != ( ToProcessSessoin = NetObj.GetProcessList()->GetFirstSession() ) )
        { // Something recevied from network.
            char RecvString[512];
            int SessionIndex;

            ToProcessSessoin->PacketMessage >> RecvString;
            ToProcessSessoin->PacketMessage >> SessionIndex;
            std::cout << "\n Packet Recevied: " << RecvString << SessionIndex;

            // Step 8. After finish the process with packet, You should delete the session message.
            //         If you forget to delete, it will cause memory leak!
            NetObj.GetProcessList()->DeleteFirstSession();
        }

        // From here, you can start your own rendering and game process.
        // Add (modify) your codes and process your rendering. Also add the codes for whatever you need to do.
        { // This block is a example how to make a packet and send it to the server.

            // (for test) Prepare variables to store the data from packet.
            int IntNumber;
            float FloatNumber = 56.78f; // this will convert into 56.7799988 because of floating point error.
            char StringData[] = "String Message!";
            int SendReturn;

            std::cout << "\n Input Any Number to send:";
            std::cin >> IntNumber;

            // Let's try to send a test message!
            PacketData.Reset();
            PacketData << 1234; // Add integer.
            PacketData << IntNumber;
            PacketData << FloatNumber;
            PacketData << StringData;
            SendReturn = NetObj.SendPacket( PacketData );
        }
    }


    return 0;
}