#pragma once
#ifndef _DEBUG_H
#define _DEBUG_H

class _Debug
{
    private :
        int DebugLevel;

    public :
        _Debug();
        ~_Debug();

    public :
        void log( char *Msg );
        void log( const char *Format, ... );
};

#endif