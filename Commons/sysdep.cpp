#include "sysdep.h"

int init_sysdep( void )
{
    // check data type sizes.
    if( 1 != sizeof( char ) )
    {
        return -1;
    }

    if( 2 != sizeof( short int ) )
    {
        return -1;
    }

    if( 4 != sizeof( int ) )
    {
        return -1;
    }

    if( 8 != sizeof( long long ) )
    {
        return -1;
    }

    if( 4 != sizeof( float ) )
    {
        return -1;
    }

    if( 8 != sizeof( double ) )
    {
        return -1;
    }

    return 1;
}