#pragma once
#ifndef _SYSDEP_H
#define _SYSDEP_H


#define _WINSOCK_DEPRECATED_NO_WARNINGS


// Rename the types.
typedef char               _BYTE;
typedef unsigned char      _UBYTE;
typedef short int          _INT16;
typedef unsigned short int _UINT16;
typedef int                _INT32;
typedef unsigned int       _UINT32;
typedef long long          _INT64;
typedef unsigned long long _UINT64;
typedef float              _FLOAT32;
typedef double             _DOUBLE64;

typedef unsigned short int _PACKET_ID; // Data type for packet id.

int init_sysdep( void );

#endif
