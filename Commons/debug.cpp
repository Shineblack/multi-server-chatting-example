#include <iostream>
#include <stdarg.h>
#include "debug.h"

_Debug::_Debug()
{
    DebugLevel = 0;
}

_Debug::~_Debug()
{
}

void _Debug::log( char * Msg )
{
    this->log( (const char *)Msg );
}

void _Debug::log( const char *Format, ... )
{
    // ** WARNING **
    // If you want to run this program in the Windows Service Mode, you should make proper log function.
    // This function is only for this example and printing the message on the console only.

    char Msg[1024];
    va_list Args;

    if( NULL == Format ) return;

    va_start( Args, Format );
    vsprintf_s( Msg, Format, Args );

    /*
    char msg[1024];
    va_list args;

    va_start(args, format);
    if( format == NULL ) return FALSE;
    vsprintf(msg, format, args);

    write(1, msg, strlen(msg));

    va_end(args);
    */

#ifdef _DEBUG
    std::cout << Msg << std::endl;
#endif

    va_end( Args );
}