#pragma once
#ifndef _CLIENT_PACKETS_H
#define _CLIENT_PACKETS_H


#include "sysdep.h"


/**
* This file defines the packet details of the Masater Server to Client.
*/
// Packet ID. Master Server -> Client.
enum
{
    PACKET_ID_M2C_NONE = 0,
    PACKET_ID_M2C_MESSAGE_SERVER_TO_CONNECT,

    PACKET_ID_M2C_MAX
};

#pragma pack(push, 1)
struct _PKT_M2C_MESSAGE_SERVER_TO_CONNECT
{
    _UINT16 PortNumber;       // If the PortNumber is 0 than no Message Server to connect.
    _BYTE   IPAddress[16];    // "xxx.xxx.xxx.xxx"
};
#pragma pack(pop)


// Packet ID. Client -> Message Server.
enum
{
    PACKET_ID_C2G_NONE = 0,
    PACKET_ID_C2G_CHATMESSAGE,

    PACKET_ID_C2G_MAX
};

#pragma pack(push, 1)
#define C2G_CHATMESSAGE_MESSAGELENGTH_MAX 1024 // Must less than MTU (1500).
struct _PKT_C2G_CHATMESSAGE
{
    _UINT16 MessageLength;
    _BYTE   *Message;
};
#pragma pack(pop)

// PacketID. Message Server -> Client.
enum
{
    PACKET_ID_G2C_NONE = 0,
    PACKET_ID_G2C_CHATMESSAGE,

    PACKET_ID_G2C_MAX
};

#pragma pack(push, 1)
struct _PKT_G2C_CHATTOMESSAGE
{
    _UINT16 MessageLength;
    _BYTE   *Message;
};
#pragma pack(pop)

// To make short and easy to call packet processing function.
#define CPP_FUNC(dir, name) \
    packet_process_##dir_##name( struct HNet::_ProcessSession *ToProcessSession )
#define CPP_FUNC_CALL(dir, name) \
    packet_process_##dir_##name( ToProcessSession )
#define CPP_FUNC_NAME(dir, name) packet_process_##dir_##name

#endif