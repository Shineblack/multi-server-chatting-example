#pragma once
#ifndef _CLIENT_H
#define _CLIENT_H

#include <map>

#include "client_packets.h"
#include "ClientNetwork.h"

enum
{
    CLIENT_STATE_NOTREADY = 0,
    CLIENT_STATE_CONNECTED_TO_MASTERSERVER,
    CLIENT_STATE_CONNECTED_TO_MESSAGESERVER,

    CLIENT_STATE_MAX
};

class _Client
{
    private :

        HNet::_ClientNetwork NetObjMaster;

        HNet::_ClientNetwork NetObjMessage;
        char MessageServerIPAddress[16];
        unsigned int MessageServerPortNumber;

        int ClientState;

    public :
        _Client() {};
        ~_Client() {};

    private : // Setters.
        void SetMessageServerIPAddress( const char *IPAddress );
        void SetMessageServerPortNumber( unsigned int PortNumber ) { MessageServerPortNumber = PortNumber; }

        void SetClientState( int State ) { this->ClientState = State; };
    public : // Getter.
        char *GetMessageServerIPAddress( void ) { return MessageServerIPAddress; }
        unsigned int GetMessageServerPortNumber( void ) { return MessageServerPortNumber; }

        int GetClientState( void ) { return this->ClientState; }

    public :
        void Init( void );
        int RunMasterServerRecv( void );
        int RunMessageServerRecv( void );

    private :
        void CPP_FUNC( M2C, message_server_to_connect );

    private :
        void c2g_chat_message( char Mesasge[], int MessageLength );

    private :
        typedef void ( _Client::*_Func )( struct HNet::_ProcessSession *ProcessSession );
        std::map<_PACKET_ID, _Func> G2C_ProcessFunction;
        void RunG2CPacketProcess( _PACKET_ID ID, struct HNet::_ProcessSession *ProcessSession ) { ( this->*G2C_ProcessFunction[ID] )( ProcessSession ); }

        void CPP_FUNC( G2C, chat_message );
};





#endif