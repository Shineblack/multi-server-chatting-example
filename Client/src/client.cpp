// Client.
#include "client.h"

// Include the headers
#include <conio.h>  // to use _kbhit() & _getch()
#include "debug.h"

#include "client_packets.h"

// Include the header file for server.
#include "ServerNetwork.h"
#include "ClientNetwork.h"


#define DEFAULT_MASTER_SERVER_IPADDRESS "127.0.0.1"
#define DEFAULT_MASTER_SERVER_PORT 5678


// Extern.
extern _Debug Debug; // For debug.


void _Client::SetMessageServerIPAddress( const char * IPAddress )
{
    strcpy_s( this->MessageServerIPAddress, IPAddress );
}

void _Client::Init( void )
{
    // Function pointers.
    G2C_ProcessFunction[PACKET_ID_G2C_CHATMESSAGE] = &_Client::CPP_FUNC_NAME( G2C, chat_message );

    // Set initial state.
    SetClientState( CLIENT_STATE_NOTREADY );
}

int _Client::RunMasterServerRecv( void )
{
    // Prepare the pointer of _ProcessSession and buffer structure of _PacketMessage.
    struct HNet::_ProcessSession *ToProcessSession;
    _PACKET_ID PacketID;

    // Initialize the network object with necessary information.
    if( false == NetObjMaster.InitNet( HNet::APPTYPE_CLIENT, HNet::PROTOCOL_TCP, DEFAULT_MASTER_SERVER_IPADDRESS, DEFAULT_MASTER_SERVER_PORT ) )
    {
        Debug.log( NetObjMaster.GetErrorMessage() );
        return -1;
    }
    SetClientState( CLIENT_STATE_CONNECTED_TO_MASTERSERVER );
    Debug.log( "Connected to the Master Server. IPAddress[%s], PortNumber[%d]", DEFAULT_MASTER_SERVER_IPADDRESS, DEFAULT_MASTER_SERVER_PORT );

    // Main Loop.
    while( 1 )
    {
        // Message Loop.
        // Check any message from server and process.
        while( nullptr != ( ToProcessSession = NetObjMaster.GetProcessList()->GetFirstSession() ) )
        { // Something recevied from network.
            ToProcessSession->PacketMessage >> PacketID;

            switch( ToProcessSession->SessionState )
            {
                case HNet::SESSION_STATE_CLOSEREADY:
                {
                    Debug.log( "Disconnected!" );
                    NetObjMaster.GetProcessList()->DeleteFirstSession();
                    return 0;
                }
                break;

                case HNet::SESSION_STATE_READPACKET:
                {
                    Debug.log( "Packet Recevied from the Master Server! PacketID[%d]", PacketID );
                    switch( PacketID )
                    {
                        case PACKET_ID_M2C_MESSAGE_SERVER_TO_CONNECT:
                        {
                            CPP_FUNC_CALL( M2C, message_server_to_connect );
                            NetObjMaster.GetProcessList()->DeleteFirstSession();
                            return 1;
                        }
                        break; // break. switch().

                        default:
                        {
                        }
                        break;
                    }
                }
                break;

                default:
                    break;
            }
            
            NetObjMaster.GetProcessList()->DeleteFirstSession();
        }

        Sleep( 100 ); // Prevent too fast looping.
    }

    return -1;
}

int _Client::RunMessageServerRecv( void )
{
    struct HNet::_ProcessSession *ToProcessSession;
    _PACKET_ID PacketID;

    char ChatMessage[C2G_CHATMESSAGE_MESSAGELENGTH_MAX] = { '\0', };
    int ChatMessageLength = 0;

    // Connect to the Message Server.
    if( false == NetObjMessage.InitNet( HNet::APPTYPE_CLIENT, HNet::PROTOCOL_TCP, GetMessageServerIPAddress(), GetMessageServerPortNumber()) )
    {
        Debug.log( "Fail to connect to the Message Server." );
        Debug.log( NetObjMessage.GetErrorMessage() );
        return -1;
    }
    SetClientState( CLIENT_STATE_CONNECTED_TO_MESSAGESERVER );
    Debug.log( "Connected to the Message Server. IP[%s], Port[%d]", NetObjMessage.GetSessionIPAddress(), NetObjMessage.GetSessionPortNumber() );

    // Main Loop.
    while( 1 )
    {
        // Message Loop.
        // Check any message from server and process.
        while( nullptr != ( ToProcessSession = NetObjMessage.GetProcessList()->GetFirstSession() ) )
        { // Something recevied from network.
            ToProcessSession->PacketMessage >> PacketID;

            switch( ToProcessSession->SessionState )
            {
                case HNet::SESSION_STATE_CLOSEREADY:
                {
                    Debug.log( "Disconnected!" );
                    NetObjMessage.GetProcessList()->DeleteFirstSession();
                    return 0;
                }
                break;

                case HNet::SESSION_STATE_READPACKET:
                {
                    RunG2CPacketProcess( PacketID, ToProcessSession );
                }
                break;

                default:
                    break;
            }

            NetObjMessage.GetProcessList()->DeleteFirstSession();
        }

        if( _kbhit() )
        {
            char InputKey = (char)_putch(_getch());
            if( ( EOF == (int)InputKey ) || ( '\n' == InputKey ) || ( '\r' == InputKey ) )
            { // 'Enter' pressed.
                c2g_chat_message( ChatMessage, ChatMessageLength );

                memset( ChatMessage, '\0', C2G_CHATMESSAGE_MESSAGELENGTH_MAX );
                ChatMessageLength = 0;
            }
            else
            {
                ChatMessage[ChatMessageLength] = InputKey;
                ++ChatMessageLength;
                if( C2G_CHATMESSAGE_MESSAGELENGTH_MAX <= ChatMessageLength )
                {
                    c2g_chat_message( ChatMessage, ChatMessageLength );

                    memset( ChatMessage, '\0', C2G_CHATMESSAGE_MESSAGELENGTH_MAX );
                    ChatMessageLength = 0;
                }
            }
        }

        Sleep( 100 ); // Prevent too fast looping.
    }

    return -1;
}
