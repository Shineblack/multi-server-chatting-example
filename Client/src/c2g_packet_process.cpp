#include "client.h"
#include "client_packets.h"

#include "ClientNetwork.h"

#include "debug.h"

// Extern.
extern _Debug Debug;

// Send chat message to the Message Server.
void _Client::c2g_chat_message( char Message[], int MessageLength )
{
    if( ( 0 >= MessageLength ) || ( C2G_CHATMESSAGE_MESSAGELENGTH_MAX <= MessageLength ) )
    {
        Debug.log( "Chat message length error! Too short or too long. Length[%d]", MessageLength );
        return;
    }

    struct _PKT_C2G_CHATMESSAGE SendData;
    SendData.MessageLength = MessageLength;
    SendData.Message = new char[MessageLength];
    memcpy( SendData.Message, Message, MessageLength );

    struct HNet::_PacketMessage PacketData;
    _PACKET_ID PacketID = PACKET_ID_C2G_CHATMESSAGE;
    PacketData << PacketID;
    PacketData << SendData.MessageLength;
    PacketData.Write( (char *)SendData.Message, (int)SendData.MessageLength );

    NetObjMessage.SendPacket( PacketData );
    Debug.log( "Send: Chat message sent to the Message Server: [%d:%s]", SendData.MessageLength, Message );

    delete SendData.Message;
}