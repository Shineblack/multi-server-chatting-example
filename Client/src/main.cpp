// This is the codes for Server Programmer test.

#include "debug.h"
#include "sysdep.h"
#include "client.h"

// Global.
_Debug Debug; // For debug.

void read_runtime_arguments( int arg_count, char **arg_values )
{
    // -p PORT_NUMBER : Port number for client connection.
    // -a PORT_NUMBER : Port number for admin connection (between servers - master server and message servers).
    // -m MAX_CONNECTION : Maximum connection of clients for each message servers.

}

int main( int argc, char **argv )
{
    _Client ThisClient;

    if( 1 != init_sysdep() )
    {
        Debug.log( "Wrong size of default data types!" );
        return 0;
    }

    if( 1 < argc )
    {
        read_runtime_arguments( argc, argv );
    }

    ThisClient.Init();

    while( 1 )
    {
        Debug.log( "Trying to connect to the Master Server..." );
        if( 0 >= ThisClient.RunMasterServerRecv() )
        {
            break; // Cannot connect to the Master Server.
        }
        if( 0 > ThisClient.RunMessageServerRecv() )
        { // Disconnected from Message Server because of any error!
            break; // Stop.
        }
        Debug.log( "Disconnected from Message server and retry to conntect to the Master Server." );
    }

    return 0;
}