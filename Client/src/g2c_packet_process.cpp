#include <iostream>
#include "client.h"
#include "client_packets.h"

#include "debug.h"


// Extern.
extern _Debug Debug;


void _Client::CPP_FUNC( G2C, chat_message )
{
    struct _PKT_G2C_CHATTOMESSAGE Received;
    ToProcessSession->PacketMessage >> Received.MessageLength;
    Received.Message = new char[Received.MessageLength]; // Quite unnecessory! but let's just leave this here.
    ToProcessSession->PacketMessage.Read( (char *)Received.Message, (int)Received.MessageLength );
    char ChatMessage[C2G_CHATMESSAGE_MESSAGELENGTH_MAX] = { '\0', };
    memcpy( ChatMessage, Received.Message, Received.MessageLength );
    Debug.log( "Received: PACKET_ID_G2C_CHATMESSAGE: Length[%d], Message[%s]", Received.MessageLength, ChatMessage );

    // Print on the screen.
    std::cout << "Chatting message received: " << ChatMessage << std::endl;

    delete Received.Message;
}