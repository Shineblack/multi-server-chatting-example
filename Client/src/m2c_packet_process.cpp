// Include my headers
#include "sysdep.h"
#include "debug.h"

#include "client_packets.h"
#include "client.h"


// Extern.
extern _Debug Debug;


void _Client::CPP_FUNC( M2C, message_server_to_connect )
{
    if( CLIENT_STATE_CONNECTED_TO_MASTERSERVER != GetClientState() )
    {
        return;
    }

    struct _PKT_M2C_MESSAGE_SERVER_TO_CONNECT Received;
    ToProcessSession->PacketMessage >> Received;
    Debug.log( "Packet Received from Master Server: Message Server info. IP[%s], Port[%d]", Received.IPAddress, Received.PortNumber );

    if( 0 == Received.PortNumber )
    { // Somehow cannot connect to the Message Server.
        Debug.log( "Impossible to connect to any Master Server." );
        return;
    }

    // Now, Possible to connect to the Message Server.
    // First, disconnect from master server.
//    NetObjMaster.CloseSession();
    Debug.log( "Master Server connection closed." );

    SetMessageServerIPAddress( Received.IPAddress );
    SetMessageServerPortNumber( Received.PortNumber );
}