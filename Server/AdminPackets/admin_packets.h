#pragma once
#ifndef _ADMIN_PACKETS_H
#define _ADMIN_PACKETS_H

#include "sysdep.h"

#include "ServerNetwork.h"
#include "ClientNetwork.h"


/**
* This file defines the packet details of the Server to Server packets.
*/


/**
* This is the list of Packet ID.
*   - M : Master Server
*   - G : Message Server
*   - C : Client
*/

// Packet IDs for Master Server -> Message Server Packets.
enum
{
    PACKET_ID_M2G_NONE = 0,
    PACKET_ID_M2G_ACK_REGMESSAGESERVER,
    PACKET_ID_M2G_CHATTOALL,

    PACKET_ID_M2G_MAX
};

#pragma pack(push, 1)
struct _PKT_M2G_ACK_REGMESSAGESERVER
{
    _BYTE   IsSuccess;   // true == Successfully registered and Message Server can proceed. false == Cannot proceed. Stop the server!
    _UINT16 ServerIndex; // Index number for the Message Server.
};
struct _PKT_M2G_CHATTOALL
{
    _UINT16 MessageLength;
    _BYTE   *Message;
};
#pragma pack(pop)


// Packet IDs for Message Server -> Master Server Packets.
enum
{
    PACKET_ID_G2M_NONE = 0,
    PACKET_ID_G2M_REQ_REGMESSAGESERVER, // Not using this packet ID. If message server need to send any info then use this.
    PACKET_ID_G2M_CURRENTCLIENTCOUNT,
    PACKET_ID_G2M_CHATTOALL,

    PACKET_ID_G2M_MAX
};

#pragma pack(push, 1)
struct _PKT_G2M_REQ_REGMESSAGESERVER
{
    _BYTE IPAddress[16];  // "xxx.xxx.xxx.xxx"
};
struct _PKT_G2M_CURRENTCLIENTCOUNT
{
    _UINT16 ServerIndex;
    _UINT16 CurrentClientCount;
};
struct _PKT_G2M_CHATTOALL
{
    _UINT16 MessageLength;
    _BYTE   *Message;
};
#pragma pack(pop)


// To make short and easy to call packet processing function.
#define SPP_FUNC(dir, name) \
    packet_process_##dir_##name( struct HNet::_ProcessSession *ToProcessSession )
#define SPP_FUNC_CALL(dir, name) \
    packet_process_##dir_##name( ToProcessSession )
#define SPP_FUNC_NAME(dir, name) packet_process_##dir_##name



/*
struct _admin_packet_process
{
    _PACKET_ID PacketID;
    void( *process_function )( HNet::_ServerNetwork *NetServerObj, struct HNet::_ProcessSession *ProcessSession );
};

struct _client_packet_process
{
    _PACKET_ID PacketID;
    void( *process_function )( struct HNet::_ProcessSession *ProcessSession );
};
*/


#endif