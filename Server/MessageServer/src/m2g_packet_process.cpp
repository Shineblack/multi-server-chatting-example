#include "message_server.h"
#include "admin_packets.h"

void _MessageServer::SPP_FUNC( M2G, ack_reg_message_server )
{
    struct _PKT_M2G_ACK_REGMESSAGESERVER Received;

    ToProcessSession->PacketMessage >> Received;
    if( true == (bool)Received.IsSuccess )
    {
        Debug.log( "Message Server registration successed. Index number of this server is [%d]", Received.ServerIndex );
        SetServerIndex( Received.ServerIndex );

        // Initialize the client network object with necessary information.
        if( false == NetObjClient.InitNet( HNet::SOCKET_MODEL_SELECT, HNet::PROTOCOL_TCP, ( DEFAULT_CLIENT_PORT + Received.ServerIndex ) ) )
        {
            Debug.log( NetObjClient.GetErrorMessage() );
            return;
        }
        Debug.log( "Server network for client connection has .initialized! PortNumber[%d]", NetObjClient.GetPortNumber() );

        SetServerState( SERVER_STATE_RUNNING );
    }
    else
    {
        Debug.log( "Message Server registration failed!" );
    }
}

void _MessageServer::SPP_FUNC( M2G, chat_to_all )
{
    struct _PKT_M2G_CHATTOALL Received;
    ToProcessSession->PacketMessage >> Received.MessageLength;
    Received.Message = new char[Received.MessageLength];
    ToProcessSession->PacketMessage.Read( (char *)Received.Message, (int)Received.MessageLength );
#ifdef _DEBUG
    char ChatMessage[C2G_CHATMESSAGE_MESSAGELENGTH_MAX] = { '\0', };
    memcpy( ChatMessage, Received.Message, Received.MessageLength );
    Debug.log( "Received: PACKET_TO_M2C_CHATTOALL: Length[%d], Message[%s]", Received.MessageLength, ChatMessage );
#endif

    // Send to all Client.
    struct HNet::_PacketMessage PacketData;
    _PACKET_ID PacketID = PACKET_ID_G2C_CHATMESSAGE;
    PacketData << PacketID;
    PacketData << Received.MessageLength;
    PacketData.Write( (char*)Received.Message, (int)Received.MessageLength );
    NetObjClient.SendPacketToAll( PacketData );
    Debug.log( "Send: PACKET_ID_G2C_CHATMESSAGE: Length[%d]", Received.MessageLength );

    delete Received.Message;
}