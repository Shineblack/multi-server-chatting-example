#pragma once
#ifndef _MESSAGE_SERVER_H
#define _MESSAGE_SERVER_H

#include <thread>
#include <map>
#include <string>

#include "debug.h"

#include "admin_packets.h"
#include "client_packets.h"

#define DEFAULT_CLIENT_PORT 7890

// Server state.
enum
{
    SERVER_STATE_NOTREADY = 0,
    SERVER_STATE_WAIT_MASTER_SERVER,
    SERVER_STATE_RUNNING,
    SERVER_STATE_CLOSEDOWN
};

class _MessageServer
{
    private:
        int ServerIndex;

        // Server network object to use network library.
        HNet::_ServerNetwork NetObjClient;
        HNet::_ClientNetwork NetObjServerAdmin;

        unsigned short int ConnectedClientsCount;
        int ServerState;

    public:
        _Debug Debug; // for Debug.

    private: // Setters.
        void   SetServerIndex         ( int Index ) { this->ServerIndex = Index;       }
        void   SetConnectedClientCount( unsigned short int Number ) { this->ConnectedClientsCount = Number; }
        void   AddConnectedClientCount( void      ) { ++( this->ConnectedClientsCount ); }
        void   DelConnectedClientCount( void      ) { --( this->ConnectedClientsCount ); }
        void   SetServerState         ( int State ) { this->ServerState = State;       }

    protected: // Getters.
        int    GetServerIndex       ( void      ) { return this->ServerIndex;        }
        unsigned short int GetConnectedClientCount( void ) { return this->ConnectedClientsCount; }
        int    GetServerState       ( void      ) { return this->ServerState;        }

    public:
        void Init( void );
        void Run( void );

    public:
        _MessageServer() { ServerState = SERVER_STATE_NOTREADY; ConnectedClientsCount = 0; }
        ~_MessageServer() {}

    private:
        typedef void (_MessageServer::*_Func)( struct HNet::_ProcessSession *ProcessSession );
        std::map<_PACKET_ID, _Func> M2G_ProcessFunction;
        std::map<_PACKET_ID, _Func> C2G_ProcessFunction;
        void RunM2GPacketProcess( _PACKET_ID ID, struct HNet::_ProcessSession *ProcessSession ) { ( this->*M2G_ProcessFunction[ID] )( ProcessSession ); }
        void RunC2GPacketProcess( _PACKET_ID ID, struct HNet::_ProcessSession *ProcessSession ) { ( this->*C2G_ProcessFunction[ID] )( ProcessSession ); }

    private: // Function prototypes. Master Server -> Message Server.
        void SPP_FUNC( M2G, ack_reg_message_server );
        void SPP_FUNC( M2G, chat_to_all );

    private: // Function prototypes. Message Server -> Master Server.
        void SPP_FUNC( G2M, current_client_connected );

    private: // Function prototypes. Client -> Message Server.
        void CPP_FUNC( C2G, chat_message );
};



#endif
