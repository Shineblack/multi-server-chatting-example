#include "message_server.h"
#include "admin_packets.h"


void _MessageServer::SPP_FUNC( G2M, current_client_connected )
{
    struct _PKT_G2M_CURRENTCLIENTCOUNT SendData;
    SendData.ServerIndex = GetServerIndex();
    SendData.CurrentClientCount = GetConnectedClientCount();

    struct HNet::_PacketMessage PacketData;
    _PACKET_ID PacketID = PACKET_ID_G2M_CURRENTCLIENTCOUNT;
    PacketData << PacketID;
    PacketData << SendData;

    NetObjServerAdmin.SendPacket( PacketData );
    Debug.log( "Sent current client count number to the Master Server. Current connected client number is [%d]", GetConnectedClientCount() );
}