// Message Server.

#include <thread>

// Include my headers
#include "sysdep.h"
#include "debug.h"
#include "message_server.h"

// Include the header file for server.
#include "admin_packets.h"
#include "client_packets.h"
#include "ServerNetwork.h"
#include "ClientNetwork.h"


void _MessageServer::Init( void )
{
    // Assign function pointers. Need to check Array Index and PacketID is same or not.
    // Function pointers. Master Server -> Message Server.
    M2G_ProcessFunction[PACKET_ID_M2G_ACK_REGMESSAGESERVER] = &_MessageServer::SPP_FUNC_NAME( M2G, ack_reg_message_server );
    M2G_ProcessFunction[PACKET_ID_M2G_CHATTOALL]            = &_MessageServer::SPP_FUNC_NAME( M2G, chat_to_all );

    // Function pointers. Clinet -> Message Server.
    C2G_ProcessFunction[PACKET_ID_C2G_CHATMESSAGE] = &_MessageServer::CPP_FUNC_NAME( C2G, chat_message );
}

void _MessageServer::Run( void )
{
    // Initialize the network object with necessary information.
    if( false == NetObjServerAdmin.InitNet( HNet::SOCKET_MODEL_SINGLETHREAD, HNet::PROTOCOL_TCP, "127.0.0.1", 6789 ) )
    {
        Debug.log( NetObjServerAdmin.GetErrorMessage() );
        return;
    }
    Debug.log( "Message server connected to Master server." );
    SetServerState(SERVER_STATE_WAIT_MASTER_SERVER);

    struct HNet::_ProcessSession *ToProcessSession;
    _PACKET_ID PacketID;

    while( 1 )
    { // Server loop.
        // Check the admin message.
        while( nullptr != ( ToProcessSession = NetObjServerAdmin.GetProcessList()->GetFirstSession() ) )
        { // Something recevied from network.
            ToProcessSession->PacketMessage >> PacketID;
            switch( ToProcessSession->SessionState )
            {
                case HNet::SESSION_STATE_NEWCONNECTION:
                {
                }
                break;

                case HNet::SESSION_STATE_CLOSEREADY:
                {// Master Server closed!
                    Debug.log( "Master Server disconnect the communication!" );
                    return;
                }
                break;

                case HNet::SESSION_STATE_READPACKET:
                {
                    Debug.log( "Reaceived Admin Packet: PacketID[%d]", PacketID );
                    if( ( PACKET_ID_M2G_NONE < PacketID ) && ( PACKET_ID_M2G_MAX > PacketID ) )
                    { // ID is safe!
                        RunM2GPacketProcess( PacketID, ToProcessSession );
                    }
                    else
                    { // PacketID error!
                        Debug.log( "PacketID error!" );
                    }
                }
                break;

                default:
                break;
            }
            
            NetObjServerAdmin.GetProcessList()->DeleteFirstSession();
        }

        // Check server state.
        if( SERVER_STATE_RUNNING != GetServerState() )
        {
            std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) ); // Prevent 100% CPU.
            continue;
        }

        // Check the client message.
        if( nullptr != ( ToProcessSession = NetObjClient.GetProcessList()->GetFirstSession() ) )
        { // something to process.
            ToProcessSession->PacketMessage >> PacketID;
            switch( ToProcessSession->SessionState )
            {
                case HNet::SESSION_STATE_NEWCONNECTION:
                {
                    Debug.log( "New Client Connected!" );
                    AddConnectedClientCount();
                    SPP_FUNC_CALL( G2M, current_client_connected );
                }
                break;

                case HNet::SESSION_STATE_CLOSEREADY:
                {
                    Debug.log( "Client disconnected. Index[%d]. Current connection now[%d]", ToProcessSession->SessionIndex, NetObjClient.GetConnectedCount() );
                    DelConnectedClientCount();
                    SPP_FUNC_CALL( G2M, current_client_connected );
                }
                break;

                case HNet::SESSION_STATE_READPACKET:
                {
                    Debug.log( "Received Client Packet: PacketID[%d]", PacketID );
                    if( ( PACKET_ID_C2G_NONE < PacketID ) && ( PACKET_ID_C2G_MAX > PacketID ) )
                    {
                        RunC2GPacketProcess( PacketID, ToProcessSession );
                    }
                    else
                    {
                        Debug.log( "PacketID error!" );
                    }
                }
                break;

                default:
                break;
            }

            NetObjClient.GetProcessList()->DeleteFirstSession();
        }
        else
        { // Something to process.
            std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) ); // Prevent 100% CPU.
        }
    }
}
