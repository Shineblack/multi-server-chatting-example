#include "sysdep.h"
#include "debug.h"

#include "message_server.h"
#include "client_packets.h"

void _MessageServer::CPP_FUNC( C2G, chat_message )
{
    struct _PKT_C2G_CHATMESSAGE Received;
    ToProcessSession->PacketMessage >> Received.MessageLength;
    Received.Message = new char[Received.MessageLength];
    ToProcessSession->PacketMessage.Read( (char *)Received.Message, (int)Received.MessageLength );
#ifdef _DEBUG
    char ChatMessage[C2G_CHATMESSAGE_MESSAGELENGTH_MAX] = { '\0', };
    memcpy( ChatMessage, Received.Message, Received.MessageLength );
    Debug.log( "Received from the Client: Chat message. Length[%d], Message[%s]", Received.MessageLength, ChatMessage );
#endif

    struct HNet::_PacketMessage PacketData;
    _PACKET_ID PacketID = PACKET_ID_G2M_CHATTOALL;
    PacketData << PacketID;
    PacketData << Received.MessageLength;
    PacketData.Write( (char *)Received.Message, (int)Received.MessageLength );

    NetObjServerAdmin.SendPacket( PacketData );
    Debug.log( "Send PACKET_ID_G2M_CHATTOALL" );


    delete Received.Message;
}