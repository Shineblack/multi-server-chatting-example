// Message Server.
// This is the codes for Server Programmer test.

#include "message_server.h"


void read_runtime_arguments( int arg_count, char **arg_values )
{
    // -p PORT_NUMBER : Port number for client connection.
    // -a PORT_NUMBER : Port number for admin connection (between servers - master server and message servers).
    // -m MAX_CONNECTION : Maximum connection of clients for each message servers.

}

int main( int argc, char **argv )
{
    _MessageServer ThisServer;

    // Read and store the runtime arguments.
    if( 1 < argc )
    {
        read_runtime_arguments( argc, argv );
    }

    ThisServer.Init();
    ThisServer.Run();

    return 0;
}