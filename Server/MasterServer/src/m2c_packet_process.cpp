#include "sysdep.h"
#include "debug.h"

#include "client_packets.h"
#include "master_server.h"

#include "ServerNetwork.h"


void _MasterServer::CPP_FUNC( M2C, new_client_connected )
{
    struct _PKT_M2C_MESSAGE_SERVER_TO_CONNECT AckData;

    struct HNet::_PacketMessage PacketData;
    _PACKET_ID PacketID = PACKET_ID_M2C_MESSAGE_SERVER_TO_CONNECT;
    PacketData << PacketID;

    int MostEmptyServerIndex;

    AckData.PortNumber = 0;
    
    MostEmptyServerIndex = check_most_empty_message_server();
    if( -1 == MostEmptyServerIndex )
    { // Cannot connect to any Message Server
        AckData.IPAddress[0] = '\0';
        PacketData << AckData;
        NetObjClient.SendPacket( ToProcessSession->SessionIndex, PacketData );
        NetObjClient.CloseSession( NetObjClient.GetSessionNodeByIndex( ToProcessSession->SessionIndex ) );
        Debug.log( "No Empty Message Server! Client Connection Closed!" );
        return;
    }

    AckData.PortNumber = MessageServerList[MostEmptyServerIndex].PortNumber;
    strcpy_s( AckData.IPAddress, MessageServerList[MostEmptyServerIndex].IPAddress );
    PacketData << AckData;

    NetObjClient.SendPacket( ToProcessSession->SessionIndex, PacketData );
    NetObjClient.CloseSession( NetObjClient.GetSessionNodeByIndex( ToProcessSession->SessionIndex ) );
    Debug.log( "Sent the Message Server info to the Client. Message Server [%d], PortNumber[%d], IPAddress[%s]", MostEmptyServerIndex,
               MessageServerList[MostEmptyServerIndex].PortNumber, MessageServerList[MostEmptyServerIndex].IPAddress );
}
