// Master Server.
// This is the codes for Server Programmer test.


// Include the headers
#include "sysdep.h"
#include "debug.h"

// Include the header file for server.
#include "admin_packets.h"
#include "client_packets.h"
#include "ServerNetwork.h"

#include "master_server.h"


void _MasterServer::Init( void )
{
    // Init the memories.
    memset( MessageServerList, '\0', sizeof( struct _message_server_list ) * MAX_MESSAGE_SERVER );

    // Assign the function pointers to process the packet from the Message Server.
    G2M_ProcessFunction[PACKET_ID_G2M_REQ_REGMESSAGESERVER] = &_MasterServer::SPP_FUNC_NAME( G2M, req_reg_message_server );
    G2M_ProcessFunction[PACKET_ID_G2M_CURRENTCLIENTCOUNT]   = &_MasterServer::SPP_FUNC_NAME( G2M, current_client_count );
    G2M_ProcessFunction[PACKET_ID_G2M_CHATTOALL]            = &_MasterServer::SPP_FUNC_NAME( G2M, chat_to_all );
}

void _MasterServer::Run( void )
{
    // Initialize the network object with necessary information.
    if( false == NetObjServerAdmin.InitNet( HNet::SOCKET_MODEL_SELECT, HNet::PROTOCOL_TCP, DEFAULT_ADMIN_PORT ) )
    {
        Debug.log( NetObjServerAdmin.GetErrorMessage() );
        return;
    }
    Debug.log( "Server network for admin connection (server communication) has initialized!" );

    // Initialize the network object with necessary information.
    if( false == NetObjClient.InitNet( HNet::SOCKET_MODEL_SELECT, HNet::PROTOCOL_TCP, DEFAULT_CLIENT_PORT ) )
    {
        Debug.log( NetObjClient.GetErrorMessage() );
        return;
    }
    Debug.log( "Server network for client connection has initialized!" );

    struct HNet::_ProcessSession *ToProcessSession;
    _PACKET_ID PacketID;

    while( 1 )
    { // Server loop.
      // Check the admin message.
        while( nullptr != ( ToProcessSession = NetObjServerAdmin.GetProcessList()->GetFirstSession() ) )
        { // Something recevied from network.
            ToProcessSession->PacketMessage >> PacketID;
            switch( ToProcessSession->SessionState )
            {
                case HNet::SESSION_STATE_NEWCONNECTION:
                {
                    // New connection request arrived.
                    Debug.log( "New Message Server connected: Index:%d. Total Connection now:%d", ToProcessSession->SessionIndex, NetObjServerAdmin.GetConnectedCount() );
                    SPP_FUNC_CALL( M2G, new_message_server );
                }
                break;

                case HNet::SESSION_STATE_CLOSEREADY:
                {
                    // Connection closed arrived or communication error.
                    delete_message_server( ToProcessSession );
                    Debug.log( "Received: Index %d Message Server wants to close or already closed. Total Connection now:%d", ToProcessSession->SessionIndex, NetObjServerAdmin.GetConnectedCount() );
                    // ** CAUTION ** Don't close the connection here. Library is managing the connection.
                }
                break;

                case HNet::SESSION_STATE_READPACKET:
                {
                    // Any packet data recevied.
                    Debug.log( "Received: Admin packet received. PacketID[%d]", PacketID );
                    if( ( PACKET_ID_G2M_NONE < PacketID ) && ( PACKET_ID_G2M_MAX > PacketID ) )
                    {
                        RunG2MPacketProcess( PacketID, ToProcessSession );
                    }
                    else
                    {
                        Debug.log( "Admin packet received. But wrong PacketID [%d]", PacketID );
                    }
                }
                break;

                default:
                break;
            }

            NetObjServerAdmin.GetProcessList()->DeleteFirstSession();
        }

        // Check the client message.
        if( nullptr != ( ToProcessSession = NetObjClient.GetProcessList()->GetFirstSession() ) )
        { // something to process.
            ToProcessSession->PacketMessage >> PacketID;
            switch( ToProcessSession->SessionState )
            {
                case HNet::SESSION_STATE_NEWCONNECTION:
                {
                    // New connection request arrived.
                    Debug.log( "New Client connected: Index:%d. Total Connection now:%d", ToProcessSession->SessionIndex, NetObjClient.GetConnectedCount() );
                    CPP_FUNC_CALL( M2C, new_client_connected );
                }
                break;

                case HNet::SESSION_STATE_CLOSEREADY:
                {
                    // Connection closed arrived or communication error.
                    Debug.log( "Received: Client Index %d wants to close or already closed. Total Connection now:%d", ToProcessSession->SessionIndex, NetObjClient.GetConnectedCount() );
                    // ** CAUTION ** Don't close the client connection here. Library is managing the connection.
                }
                break;

                case HNet::SESSION_STATE_READPACKET:
                {
                    Debug.log( "Received Client Packet: PacketID[%d]", PacketID );
                    // ** NOTE ** Nothing to do yet!
                }
                break;

                default:
                break;
            }

            NetObjClient.GetProcessList()->DeleteFirstSession();
        }

        std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) ); // Prevent 100% CPU.
    }
}