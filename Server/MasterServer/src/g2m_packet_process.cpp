#include "debug.h"
#ifdef _DEBUG
#include "client_packets.h"
#endif

#include "admin_packets.h"
#include "master_server.h"
#include "ServerNetwork.h"

void _MasterServer::SPP_FUNC( G2M, current_client_count )
{
    struct _PKT_G2M_CURRENTCLIENTCOUNT Received;
    ToProcessSession->PacketMessage >> Received;
    Debug.log( "Received: PACKET_ID_G2M_CURRENTCLIENTCOUNT: Server Index[%d], Current client count[%d]", Received.ServerIndex, Received.CurrentClientCount );

    if( ( MAX_MESSAGE_SERVER <= Received.ServerIndex ) )
    {
        Debug.log( "Server index number error!" );
        return;
    }
    if( ( MAX_CLIENT_CONNECTION < Received.CurrentClientCount ) )
    {
        Debug.log( "Current client count number error!" );
        return;
    }

    MessageServerList[Received.ServerIndex].ClientConnected = Received.CurrentClientCount;
    Debug.log( "Current client count number updated. Server Index[%d], Current client count[%d]", Received.ServerIndex, MessageServerList[Received.ServerIndex].ClientConnected );
}

void _MasterServer::SPP_FUNC( G2M, chat_to_all )
{
    struct _PKT_G2M_CHATTOALL Received;
    ToProcessSession->PacketMessage >> Received.MessageLength;
    Received.Message = new char[Received.MessageLength];
    ToProcessSession->PacketMessage.Read( (char*)Received.Message, (int)Received.MessageLength );
#ifdef _DEBUG
    char ChatMessage[C2G_CHATMESSAGE_MESSAGELENGTH_MAX] = { '\0', };
    memcpy( ChatMessage, Received.Message, Received.MessageLength );
    Debug.log( "Received: PACKET_TO_G2M_CHATTOALL: Length[%d], Message[%s]", Received.MessageLength, ChatMessage );
#endif

    struct HNet::_PacketMessage PacketData;
    _PACKET_ID PacketID = PACKET_ID_M2G_CHATTOALL;
    PacketData << PacketID;
    PacketData << Received.MessageLength;
    PacketData.Write( (char *)Received.Message, (int)Received.MessageLength );

    NetObjServerAdmin.SendPacketToAll( PacketData );
    Debug.log("Send to all: PACKET_ID_M2G_CHATTOALL: Length[%d]", Received.MessageLength);

    delete Received.Message;
}