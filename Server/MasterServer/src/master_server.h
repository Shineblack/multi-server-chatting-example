#pragma once
#ifndef _MASTER_SERVER_H
#define _MASTER_SERVER_H


#include <map>


#include "sysdep.h"
#include "debug.h"

#include "admin_packets.h"
#include "client_packets.h"
#include "ServerNetwork.h"


#define MAX_MESSAGE_SERVER 10 // Just a number. :-p Less than 64!
#define MAX_CLIENT_CONNECTION 50 // Pls less than 64!
#define DEFAULT_CLIENT_CONN_PORT_IN_MESSAGE_SERVER 7890
#define DEFAULT_ADMIN_PORT 6789
#define DEFAULT_CLIENT_PORT 5678


struct _message_server_list
{
    int  AdminCommSessionIndex; // if empty, == 0.
    char IPAddress[16];         // IP Address of the Message Server.
    int  PortNumber;            // Client connection Port number of the Message Server.

    int  ClientConnected;       // Number of client connected to the Message Server.
};

class _MasterServer
{
    private:
        HNet::_ServerNetwork NetObjServerAdmin; // Server network object to accept & recv the Message Server connection & messages.
        HNet::_ServerNetwork NetObjClient;      // Server network object to accept & recv the client connections & messages.
        struct _message_server_list MessageServerList[MAX_MESSAGE_SERVER];

    private:
        typedef void ( _MasterServer::*_Func )( struct HNet::_ProcessSession *ProcessSession );
        std::map<_PACKET_ID, _Func> G2M_ProcessFunction;
        void RunG2MPacketProcess( _PACKET_ID ID, struct HNet::_ProcessSession *ProcessSession ) { ( this->*G2M_ProcessFunction[ID] )( ProcessSession ); }
        // std::map<_PACKET_ID, _Func> C2G_ProcessFunction; // No Client -> Master Server packet yet!
        // void RunC2MPacketProcess( _PACKET_ID ID, struct HNet::_ProcessSession *ProcessSession ) { ( this->*C2G_ProcessFunction[ID] )( ProcessSession ); } // No Client -> Master Server packet yet!

    private:
        _Debug Debug; // For debug.

    public:
        _MasterServer() {}
        ~_MasterServer() {}

    public:
        void Init( void );
        void Run( void );

    private: // Function prototypes. Master Server -> Message Server.
        void SPP_FUNC( M2G, new_message_server );
        int  check_most_empty_message_server( void );
        int  add_new_message_server( struct HNet::_ProcessSession *ToProcessSession );
        int  delete_message_server( struct HNet::_ProcessSession *ToProcessSession );

    private: // Function prototypes. Message Server -> Master Server.
        void SPP_FUNC( G2M, none ) {}
        void SPP_FUNC( G2M, req_reg_message_server ) {} // ** NOTE ** Not using this packet yet.
        void SPP_FUNC( G2M, current_client_count );
        void SPP_FUNC( G2M, chat_to_all );

    private: // Function prototypes. Master Server -> Client.
        void CPP_FUNC( M2C, new_client_connected );
};


// Function prototypes.
int check_most_empty_message_server( void );

#endif