#include "sysdep.h"
#include "debug.h"

#include "admin_packets.h"
#include "master_server.h"

#include "ServerNetwork.h"


void _MasterServer::SPP_FUNC( M2G, new_message_server )
{
    struct HNet::_PacketMessage PacketData;
    _PACKET_ID PacketID = PACKET_ID_M2G_ACK_REGMESSAGESERVER;
    struct _PKT_M2G_ACK_REGMESSAGESERVER AckData;
    char IsSuccess = false;
    unsigned short int ServerIndex;

    if( 0 <= ( ServerIndex = add_new_message_server( ToProcessSession ) ) ) IsSuccess = true;

    AckData.IsSuccess = IsSuccess;
    AckData.ServerIndex = ServerIndex;

    PacketData << PacketID;
    PacketData << AckData;
    NetObjServerAdmin.SendPacket( ToProcessSession->SessionIndex, PacketData );
    Debug.log( "Send Ack: REGMESSAGESERVER: IsSuccess[%d], ServerIndex[%d], IPAddress[%s], PortNumber[%d]",
               AckData.IsSuccess, AckData.ServerIndex, MessageServerList[ServerIndex].IPAddress, MessageServerList[ServerIndex].PortNumber );

    if( false == IsSuccess )
    {
        Debug.log( "Cannot register the new Message Server!" );
        NetObjServerAdmin.CloseSession( NetObjServerAdmin.GetSessionNodeByIndex( ToProcessSession->SessionIndex ) );
    }
}

int _MasterServer::check_most_empty_message_server( void )
{
    int Index;
    int MostEmptyIndex = -1;

    for( Index = 0; Index < MAX_MESSAGE_SERVER; ++Index )
    {
        if( 0 != MessageServerList[Index].AdminCommSessionIndex )
        {
            if( 0 == MessageServerList[Index].ClientConnected )
            {
                MostEmptyIndex = Index;
                break;
            }
            else
            {
                if( -1 == MostEmptyIndex )
                {
                    MostEmptyIndex = Index;
                }
                else if( MessageServerList[Index].ClientConnected < MessageServerList[MostEmptyIndex].ClientConnected )
                {
                    MostEmptyIndex = Index;
                }
            }
        }
    }

    if( 0 == MessageServerList[MostEmptyIndex].AdminCommSessionIndex )
    {
        return -1;
    }

    return MostEmptyIndex;
}

int _MasterServer::add_new_message_server( struct HNet::_ProcessSession *ToProcessSession )
{
    int Index;

    for( Index = 0; Index < MAX_MESSAGE_SERVER; ++Index )
    {
        if( 0 == MessageServerList[Index].AdminCommSessionIndex )
        {
            MessageServerList[Index].AdminCommSessionIndex = ToProcessSession->SessionIndex;
            strcpy_s( MessageServerList[Index].IPAddress, inet_ntoa( ToProcessSession->SessionAddress.sin_addr ) );
            MessageServerList[Index].PortNumber = DEFAULT_CLIENT_CONN_PORT_IN_MESSAGE_SERVER + Index;
            return Index;
        }
    }

    return -1;
}

int _MasterServer::delete_message_server( struct HNet::_ProcessSession *ToProcessSession )
{
    int Index;

    for( Index = 0; Index < MAX_MESSAGE_SERVER; ++Index )
    {
        if( ToProcessSession->SessionIndex == MessageServerList[Index].AdminCommSessionIndex )
        {
            MessageServerList[Index].AdminCommSessionIndex = 0;
            return Index;
        }
    }

    return -1;
}
